//
//  toast.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/30.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class toast {
    
    /**
     Displays a toast notification to indicate there was a failure setting up a view controller.
     
     Typically used to indicate that there was an error with downloading some data.
    */
    
    func VCInitFail(_ viewController: UIViewController){
        basic(viewController, message: "Failed to setup screen")
    }
    
    /**
     Displays a toast notification based on the boolean passed in.
     - Returns: Returns a toast notification. If Bool is true text will be success, else failed.
    */
    func success(_ viewController: UIViewController, success: Bool){
        
        if let nav = viewController.navigationController {
            success ? basic(nav, message: "Success!"): basic(viewController, message: "Failed.")
        } else {
            success ? basic(viewController, message: "Success!") : basic(viewController, message: "Failed.")
        }
        
    }
    
    /**
     Displays a basic toast notification with custom text.
     
     - NOTE: Will display toast notification provided the user still has an available navigation controller.
     */
    func basic(_ navigationController: UINavigationController, message: String){
        let view = toaster(message)
        
        navigationController.view.addSubview(view)
        
        UIView.animateKeyframes(withDuration: 4, delay: 0, options: UIViewKeyframeAnimationOptions(), animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: {
                view.alpha = 0.8
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.75, relativeDuration: 1, animations: {
                view.alpha = 0
            })
            
            }, completion: { _ in
                
                view.removeFromSuperview()
        })
    }
    
    /**
     Displays a basic toast notification with custom text.
    
     - NOTE: Will only present the toast notification provided the user is still on the presentingVC at init time. 
     - NOTE: Primarily used as a fallback in case there is no available navigation controller
     */
    func basic(_ viewController: UIViewController, message: String){
        
        let view = toaster(message)
        
        viewController.navigationController != nil ? viewController.navigationController?.view.addSubview(view) : viewController.view.addSubview(view)
        
        UIView.animateKeyframes(withDuration: 4, delay: 0, options: UIViewKeyframeAnimationOptions(), animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: {
                view.alpha = 0.8
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.75, relativeDuration: 1, animations: {
                view.alpha = 0
            })
            
            }, completion: { _ in
                
                view.removeFromSuperview()
        })
        
    }
    
    fileprivate func toaster(_ message: String) -> UIView {
        let view = UIView(frame: CGRect(x: screenWidth() * 0.125, y: screenHeight() - 120, width: screenWidth() * 0.75, height: 60))
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: screenWidth() * 0.75, height: 60))
        label.adjustsFontSizeToFitWidth = true
        label.text = message
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = fonts.shared.helveticaNeueLight
        
        view.backgroundColor = UIColor.black
        view.alpha = 0.05
        view.layer.cornerRadius = 10
        
        view.addSubview(label)
        
        return view
    }
    
}
