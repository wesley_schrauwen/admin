//
//  ParentVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/28.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

/// The parent view controller for all other view controllers. Contains references to all frameworks and anything widely used throughout the app
class ParentVC: UIViewController, customDelegate {

    let network = NetworkUtilities.sharedFirebaseInstance
    
    weak var delegate: customDelegate?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ParentVC {
            vc.delegate = self
        }
    }
    
    deinit {
        print("deinit: \(self)")
    }
}
