//
//  categoryObject.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class categoryObject {
    
    fileprivate var _id: String!
    fileprivate var _title: String!
    fileprivate var _subtitle: String!
    fileprivate var _businessID: String!
    
    var businessID: String {
        return _businessID
    }
    
    var id: String {
        return _id
    }
    
    var title: String {
        return _title
    }
    
    var subtitle: String {
        return _subtitle
    }
    
    init(id: String, businessID: String, title: String, subtitle: String){
        _id = id
        _businessID = businessID
        _title = title
        _subtitle = subtitle
    }
    
}
