//
//  ReviewVC.swift
//  StackleApp
//
//  Created by Stackle005 on 9/8/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class ReviewVC: ResolveReportVC {

    @IBOutlet weak var reviewmainImage: UIImageView!
    @IBOutlet weak var reviewLbl: UILabel!
    @IBOutlet weak var resolutionBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        reviewLbl.text = report.text
    }
    
    @IBAction func resolutionBtnClicked(_ sender: AnyObject) {
        alerts().resolutionAlert(self)
    }
}
