//
//  VerificationVC.swift
//  StackleApp
//
//  Created by Stackle005 on 9/29/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class VerificationVC: ParentVC {
    
    @IBOutlet weak var Lbl1: UILabel!
    @IBOutlet weak var Lbl2: UILabel!
    @IBOutlet weak var businessId: UILabel!
    @IBOutlet weak var registrationId: UILabel!
    @IBOutlet weak var seeDocumentationBtn: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var verifyBtn: UIButton!
    
    var verification: VerificationObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        businessId.text = verification.businessID
        registrationId.text = verification.registrationID
    }
    
    // MARK: Action
    
    @IBAction func seeDocumentationBtnPressed(_ sender: AnyObject) {
        DocumentType(verification, callback: { image in
            self.image.image = image
            self.image.isHidden = false
        })
    }
    
    @IBAction func verifyBtnPressed(_ sender: AnyObject) {
        
        performSegue(withIdentifier: "saveVerificationSegue", sender: nil)
        
    }
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "saveVerificationSegue" {
            let vc = segue.destination as! SaveVerificationVC
            vc.verification = verification
        }
        
    }
    
    // MARK: convenience
    
    func DocumentType (_ data:VerificationObject, callback: @escaping (UIImage) -> Void) {
        
        let type = verification.url
        let typeExtention = Range(type.characters.index(type.endIndex, offsetBy: -3) ..< type.endIndex)
        let extention = type[typeExtention]
        print(extention)
        if extention == "jpg" {
            network.downloadImage(verification.url, callback: { image in
                self.image.image = image
                callback(image)
            })
        } else if extention == "pdf" {
            network.downloadDocument(verification.url, callback: { document in
                print(document)
            })
            print(extention)
        } else {
            print("Wrong Format -> \(extention)")
        }
    }
}
