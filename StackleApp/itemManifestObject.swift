//
//  itemManifestObject.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class itemManifestObject {
    
    fileprivate var _groupID: String!
    fileprivate var _businessID: String!
    fileprivate var _id: String!
    fileprivate var _name: String!
    fileprivate var _price: String!
    fileprivate var _detail: String!
    fileprivate var _likes: String!
    fileprivate var _url: String!
    fileprivate var _image: UIImage?
    fileprivate var _type: String!
    
    var type: String {
        return _type
    }
    
    var url: String {
        return _url
    }
    
    var image: UIImage? {
        return _image
    }
    
    var groupID: String {
        return _groupID
    }
    
    var businessID: String {
        return _businessID
    }
    
    var id: String {
        return _id
    }
    
    var name: String {
        return _name
    }
    
    var price: String {
        return _price
    }
    
    var detail: String {
        return _detail
    }
    
    var likes: String {
        return _likes
    }
    
    init(id: String, groupID: String, businessID: String, name: String, price: String, detail: String, likes: String, url: String, type: String){
        
        _id = id
        _groupID = groupID
        _businessID = businessID
        _name = name
        _price = price
        _detail = detail
        _likes = likes
        _url = url
        _type = type
        
    }
    
    func setImage(_ image: UIImage){
        self._image = image
    }
    
    func updateImage(_ image: UIImage){
        
        self._image = image
        self._url = String().createItemManifestImageURL(businessID, type: type, groupID: groupID)
        
    }
    
    func update(_ title: String, price: String, detail: String){
        _name = title
        _price = price
        _detail = detail
    }
    
}
