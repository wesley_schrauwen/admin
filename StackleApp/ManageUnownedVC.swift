//
//  ManageUnowned.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/30.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class ManageUnownedVC: unownedParentVC, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var logoView: UIButton!
    
    fileprivate var imagePicker = UIImagePickerController()
    /// Used when changing or setting the new owner of a business. Must be set by the presenting VC.
    var isUnowned = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        nameTextfield.delegate = self
        nameTextfield.text = data.name
        
        guard data.businessID != nil else {
            self.navigationController?.popViewController(animated: false)
            return
        }
        
        if let logo = data.logo {
            
            logoView.setImage(logo, for: UIControlState())
            
        } else if let businessID = data.businessID {
            
            network.downloadLogoForBusinessID(businessID, callback: { image in
                
                self.logoView.setImage(image, for: UIControlState())
                self.data.logo = image
                
            })
            
        }
        
    }
    
    // MARK: Image Picker
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        self.dismiss(animated: true, completion: nil)
        logoView.setImage(image, for: UIControlState())
        
        network.setLogo(data.businessID!, logo: image, progress: { _progress in
            
            self.logoView.alpha = CGFloat(_progress)
            
            }, success: { _success in
                
                toast().success(self, success: _success)
                
        })
        
    }
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let vc = segue.destination as? itemParentVC, let _itemType = sender as? String {
            vc.itemType = _itemType
        }
        
    }
    
    // MARK: Action
    
    @IBAction func setLogoButton(_ sender: AnyObject) {
        alerts().imagePickerActionSheet(self, imagePicker: &imagePicker)
    }
    
    @IBAction func editOwnerButton(_ sender: AnyObject) {
        alerts().textFieldAlert(self, message: alerts.alertMessages.editOwner, callback: { email in
        
            self.isUnowned ?
                
                self.network.setUnownedBusinessOwner(self.data.businessID!, email: email, callback: { success in
            
                toast().success(self, success: success)
                
            }) :
                
                self.network.changeBusinessOwner(self.data.businessID!, email: email, callback: { success in
                
                    toast().success(self, success: success)
                })
            
        })
    }
    
    @IBAction func categoryButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "categorySegue", sender: nil)
    }
    
    @IBAction func editContactDetailsButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "contactDetailsBusinessVCSegue", sender: nil)
    }
    @IBAction func editDescriptionButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "descriptionVCSegue", sender: nil)
    }
    
    @IBAction func editItemsButton(_ sender: UIButton) {
        performSegue(withIdentifier: "itemGroupVCSegue", sender: sender.currentTitle)
    }
    @IBAction func editMemberButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "membersVCSegue", sender: nil)
    }
    @IBAction func editStoresButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "storesVCSegue", sender: nil)
    }
}
