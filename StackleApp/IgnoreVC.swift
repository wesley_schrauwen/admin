//
//  IgnoreVC.swift
//  StackleApp
//
//  Created by Stackle005 on 9/22/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit
import Firebase

class IgnoreVC: ResolveReportVC {
    
    @IBOutlet weak var IgnoreLbl: UILabel!
    @IBOutlet weak var IgnoreBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logo = UIImage(named: "AppLogo")
        navigationItem.titleView = UIImageView(image: logo)
    }
    
    @IBAction func IgnoreBtnPredded(_ sender: AnyObject) {
        network.removeReport(report.reportKey)
    }
}
