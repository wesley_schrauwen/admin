//
//  unownedBusinessObject.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/28.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class unownedBusinessObject {
    
    var name: String?
    var logo: UIImage?
    var homeImage: UIImage?
    var lat: Double?
    var long: Double?
    var locationName: String?
    var storeEmail: String?
    var storePhone: String?
    var businessID: String?
    
    var mondayOpen: String?
    var mondayClose: String?
    var tuesdayOpen: String?
    var tuesdayClose: String?
    var wednesdayOpen: String?
    var wednesdayClose: String?
    var thursdayOpen: String?
    var thursdayClose: String?
    var fridayOpen: String?
    var fridayClose: String?
    var saturdayOpen: String?
    var saturdayClose: String?
    var sundayOpen: String?
    var sundayClose: String?
    
    
}
