//
//  storeContactDetailsVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/03.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class storeContactDetailsVC: storeParentVC, UITextFieldDelegate {

    @IBOutlet weak var websiteTextfield: UITextField!
    @IBOutlet weak var googleTextfield: UITextField!
    @IBOutlet weak var facebookTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        websiteTextfield.delegate = self
        googleTextfield.delegate = self
        emailTextfield.delegate = self
        phoneTextfield.delegate = self
        facebookTextfield.delegate = self
        
        network.downloadStoreContactDetails(storeData.businessID, storeID: storeData.id, callback: { data in
        
            self.websiteTextfield.text = data["website"]
            self.googleTextfield.text = data["google"]
            self.facebookTextfield.text = data["facebook"]
            self.emailTextfield.text = data["email"]
            self.phoneTextfield.text = data["phone"]
            
        })
    }
    
    // MARK: Textfield
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return textField == phoneTextfield ? string.isValidTextFieldNumber : true
        
    }

    // MARK: Action
    
    @IBAction func saveButton(_ sender: AnyObject) {
        
        guard phoneTextfield.text != "" || phoneTextfield.text != nil || emailTextfield.text != "" || emailTextfield.text != nil else {
            return
        }
        
        alerts().cautionAlert(self, callback: {
            self.network.updateStoreContactDetails([
                "business_id": self.storeData.businessID,
                "store_id": self.storeData.id,
                "title": "Store",
                "phone": self.phoneTextfield.text!,
                "email": self.emailTextfield.text!,
                "website": self.websiteTextfield.text!,
                "facebook": self.facebookTextfield.text!,
                "google": self.googleTextfield.text!
                ], callback: { success in
                    
                    toast().success(self, success: success)
                    
            })
        })
        
    }

}
