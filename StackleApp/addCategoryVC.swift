//
//  addCategoryVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class addCategoryVC: unownedParentVC, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var categories = [rawCategoryObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        network.downloadRawCategories(data.businessID!, callback: { object in
        
            if !self.categories.contains(where: { $0.id == object.id }){
                self.categories.append(object)
                self.tableView.insertRows(at: [IndexPath(row: self.categories.count - 1, section: 0)], with: .none)
            }
        
        })
        
    }
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let vc = segue.destination as? selectSubcategoryVC, let object = sender as? rawCategoryObject {
            vc.category = object
        }
        
    }
    
    // MARK: Delegate
    
    func categorySelected(_ id: String, category: String, subcategory: String) {
        self.dismiss(animated: true, completion: nil)
        delegate?.categorySelected!(id, category: category, subcategory: subcategory)
        
    }
    
    // MARK: Table View

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTVCell") {
            
            cell.textLabel?.text = categories[indexPath.row].title
            return cell
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "selectSubcategorySegue", sender: categories[indexPath.row])
    }
    
    // MARK: Action
    
    @IBAction func cancelButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }

}
