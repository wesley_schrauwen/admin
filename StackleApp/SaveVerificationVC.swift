//
//  SaveVerificationVC.swift
//  StackleApp
//
//  Created by Stackle005 on 9/29/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import UIKit

class SaveVerificationVC: ParentVC {
    
    @IBOutlet weak var Lbl1: UILabel!
    @IBOutlet weak var Lbl2: UILabel!
    @IBOutlet weak var Lbl3: UILabel!
    @IBOutlet weak var Lbl4: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var registrationId: UILabel!
    @IBOutlet weak var reasonTextLabel: UITextView!
    @IBOutlet weak var verificationBtn: UIButton!
    
    var reason: String!
    var verification: VerificationObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        businessName.text = verification.businessID
        registrationId.text = verification.registrationID
        reasonTextLabel.text = ""
    }
    
    @IBAction func verificationBtnPressed(_ sender: AnyObject) {
        reason = reasonTextLabel.text
        
        network.saveVerification(verification)
        
        
    }
}
