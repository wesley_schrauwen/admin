//
//  VerificationObject.swift
//  StackleApp
//
//  Created by Stackle005 on 9/27/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import UIKit

class VerificationObject {
    
    fileprivate var _businessID: String!
    fileprivate var _registrationID: String!
    fileprivate var _url: String!
    fileprivate var _approval: String!
    fileprivate var _reason: String!
    fileprivate var _logo: UIImage?
    
    public enum approvalTypes: String {
        case approved = "approved"
        case rejected = "rejected"
        case pending = "pending"
        case none = ""
    }
    
    var businessID : String {
        return _businessID
    }
    var registrationID : String {
        return _registrationID
    }
    var url: String {
        return _url
    }
    var approval: String {
        return _approval
    }
    var reason: String {
        return _reason
    }
    
    var logo: UIImage? {
        return _logo
    }
    
    init(businessID: String, registrationID: String, url: String, approval: approvalTypes? = nil, reason: String? = nil) {
            
        self._businessID = businessID
        self._registrationID = registrationID
        self._url = url
        
        if let _ = approval {
            self._approval = approval!.rawValue
        }
        
        
        self._reason = reason
    }
    
    func setApproval(_ approval: approvalTypes, reason: String){
        _approval = approval.rawValue
        _reason = reason
    }
    
    func setLogo(_ image: UIImage){
        _logo = image
    }
}
