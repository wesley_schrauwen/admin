//
//  customDelegate.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/28.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

@objc
protocol customDelegate {
    @objc optional func coordsSet(_ lat: Double, long: Double)
    @objc optional func businessHoursSet(_ type: String, open: String, close: String)
    /// Used primarily on items with accessory views such as toolbars to let the parent know about any action that has taken place.
    @objc optional func doneButton()
    @objc optional func buttonPushed(_ indexPath: IndexPath)
    /// Used when adding a category to a business. This can be used to determine the id which was selected and then add the category to the business.
    @objc optional func categorySelected(_ id: String, category: String, subcategory: String)
}
