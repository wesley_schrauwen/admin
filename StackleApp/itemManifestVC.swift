//
//  itemManifestVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class itemManifestVC: itemParentVC, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var items = [itemManifestObject]()
    
    var group: itemGroupObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        network.downloadItemManifest(data.businessID!, type: itemType, groupID: group.id, callback: { object in
        
            if !self.items.contains(where: { $0.id == object.id }) {
                self.items.append(object)                
                self.tableView.insertRows(at: [IndexPath(item: self.items.count - 1, section: 0)], with: .none)
            }
            
        })

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        FIREBASE_DATABASE.child("businesses/\(data.businessID!)/\(itemType)_manifest/\(group.id)/").removeAllObservers()
    }
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let vc = segue.destination as? editItemManifestVC {
            
            if let object = sender as? itemManifestObject {
                vc.item = object
            }
            
            vc.group = self.group
        }
        
        
    }
    
    // MARK: Action
    
    
    @IBAction func addItemButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "editItemManifestSegue", sender: nil)
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: "itemManifestTVCell") as? itemManifestTVCell {
            
            cell.setupCell(items[indexPath.row], indexPath: indexPath, callback: { image in
            
            self.items[indexPath.row].setImage(image)
            
            })
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        alerts().editItemAlert(self, update: {
            
            self.performSegue(withIdentifier: "editItemManifestSegue", sender: self.items[indexPath.row])
            
            }, remove: {
        
                alerts().cautionAlert(self, controllerMessage: alerts.alertMessages.remove, actionTitle: alerts.actionTitles.remove, callback: {
                
                    self.network.deleteItemManifest(self.items[indexPath.row], callback: { success in
                        toast().success(self, success: success)
                    })
                    
                })
                
        })
        
    }
}
