//
//  setBusinessLocationVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/28.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit
import MapKit

class setBusinessLocationVC: ParentVC, MKMapViewDelegate {

    @IBOutlet weak var map: MKMapView!
    fileprivate var annotation: locationAnnotation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        map.delegate = self
        map.showsBuildings = false
        map.showsUserLocation = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if annotation != nil {
            map.removeAnnotation(annotation!)
            annotation = nil
        }
    
        
        map.mapType = .standard
        map.showsUserLocation = false
        map.delegate = nil
        map.removeFromSuperview()
        map = nil
        
    }

    
    // MARK: MKMAP

    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
        print("here")
        
        annotation = locationAnnotation(title: "Location", subtitle: "Set as Business Location?", coordinate: userLocation.coordinate)
        
        mapView.addAnnotation(annotation!)
        
        let region = MKCoordinateRegion(center: userLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
        
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = false
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let pin = mapView.dequeueReusableAnnotationView(withIdentifier: "businessLocationPin") as? MKPinAnnotationView {
            
            pin.annotation = annotation
            
            return pin
            
        } else {
            
            let pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "businessLocationPin")
            pin.animatesDrop = true
            pin.canShowCallout = true
            pin.calloutOffset = CGPoint(x: -10, y: -10)
            let button = UIButton(type: .contactAdd)
            button.tag = 0
            button.addTarget(self, action: #selector(self.selectBusinessLocation(_:)), for: .touchUpInside)
            pin.rightCalloutAccessoryView = button
            
            return pin
        }
        
    }
    
    // MARK: Action
    
    @IBAction func selectBusinessLocation(_ sender: UIButton){
        
        guard annotation != nil else {
            return
        }
        
        let coords = annotation!.coordinate
        
        delegate?.coordsSet!(coords.latitude, long: coords.longitude)
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func gestureRecognizer(_ sender: UILongPressGestureRecognizer){
        
        if annotation != nil {
            map.removeAnnotation(annotation!)
        }
        
        let coords = sender.location(in: map)
        let translation = map.convert(coords, toCoordinateFrom: map)
    
        
        annotation = locationAnnotation(title: "Location", subtitle: "Set as Business Location?", coordinate: translation)
        map.addAnnotation(annotation!)
        
    }
    
    @IBAction func cancelButton(_ sender: AnyObject){
        self.dismiss(animated: true, completion: nil)
    }
    

}
