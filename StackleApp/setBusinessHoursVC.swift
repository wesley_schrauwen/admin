//
//  setBusinessHoursVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/28.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class setBusinessHoursVC: ParentVC, UITextFieldDelegate {

    
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var openHoursTextfield: UITextField!
    @IBOutlet weak var closeHoursTextfield: UITextField!
    @IBOutlet weak var picker: UIDatePicker!
    
    var hoursLabelText: String!
    
    fileprivate var selectedTextField: UITextField! {
        willSet {
            
            guard selectedTextField != nil else {
                return
            }
            
            selectedTextField.layer.borderColor = UIColor.lightGray.cgColor
            selectedTextField.layer.borderWidth = 0.5
            
            
        } didSet {
            
            guard selectedTextField != nil else {
                return
            }
            
            selectedTextField.layer.borderColor = UIColor().StackleTeal.cgColor
            selectedTextField.layer.borderWidth = 2
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        openHoursTextfield.delegate = self
        closeHoursTextfield.delegate = self
        
        openHoursTextfield.layer.cornerRadius = 5
        closeHoursTextfield.layer.cornerRadius = 5
        
        selectedTextField = openHoursTextfield
        
        hoursLabel.text = hoursLabelText.capitalized
        
    }
    
    // MARK: Textfield
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTextField = textField
        return false
    }

    // MARK: Action

    @IBAction func cancelButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButton(_ sender: AnyObject) {
        
        guard openHoursTextfield.text != "" || closeHoursTextfield.text != "" else {
            return
        }
        
        delegate?.businessHoursSet!(hoursLabel.text!, open: openHoursTextfield.text!, close: closeHoursTextfield.text!)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func pickerAction(_ sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .none
        
        
        
        selectedTextField.text = dateFormatter.string(from: sender.date)
        
    }
    
}
