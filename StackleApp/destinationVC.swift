//
//  destinationVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/01.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class destinationVC: unownedParentVC, UITextViewDelegate {

    @IBOutlet weak var textView: textViewWithToolbar!
    @IBOutlet weak var countLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.text = ""
        textView.delegate = self
        textView.toolbarDelegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        network.downloadDescription(data.businessID!, callback: { text in
            self.textView.text = text
            self.countLabel.text = "\(text.characters.count)"
        })
    }
    
    // MARK: text view
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let count = textView.text.characters.count + text.characters.count
        
        return count < 500 ? {
            self.countLabel.text = "\(count)"
            return true
            }() : false
        
    }
    
    // MARK: Delegate
    
    func doneButton() {
        network.updateDescription(data.businessID!, description: textView.text, callback: { success in
            toast().success(self, success: success)
        })
    }

}
