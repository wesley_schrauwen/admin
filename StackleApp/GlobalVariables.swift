//
//  GlobalVariables.swift
//  StackleApp
//
//  Created by Stackle005 on 8/31/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import UIKit
import Firebase
    
let FIREBASE_DATABASE = FIRDatabase.database().reference()
let FIREBASE_STORAGE = FIRStorage.storage().reference(forURL: "gs://stackl3-dc415.appspot.com")

//constants
let MAX_DOWNLOAD_SIZE: Int64 = 5 * 1024 * 1024

let compression: CGFloat = 0.75

// refrence to google app engine
let _google_engine = "https://stackle-1278.appspot.com"
let _test_engine = "http://192.168.1.121:8080"

// enum
public enum reportTypes: String {
    case business = "business"
    case comment = "comment"
    case image = "image"
    case item = "item"
    case reply = "reply"
    case review = "review"
    case none = ""
}

public enum requests: String {
    case createBusinessCategory = "/category"
    case deleteCategory = "/category/remove"
    case updateItem = "/item"
    case deleteItem = "/item/delete"
    case deleteItemGroup = "/item_group/delete"
    case updateItemGroup = "/item_group"
    case createMember = "/unowned_business/add_member"
    case deleteImage = "/image/delete"
    case createImage = "/image"
    case storeDashboard = "/dashboard"
    case createUnownedBusiness = "/add_business_no_owner"
    case setVerification = "/verification/set"
    case setUnownedBusinessOwner = "/add_business_owner"
    case changeBusinessOwner = "/change_business_owner"
    case updateDescription = "/description"
    case updateBusinessContactDetails = "/contact_details/business"
    case createStore = "/unowned_business/add_store"
    case updateLocation = "/location/update"
    case updateStoreContactDetails = "/contact_details/store"
}

/// Global font manager. Dynamically sets text size based on font size
class fonts {
    
    static let shared = fonts()
    
    var standardTextSize: CGFloat!
    
    init(){
        
        if screenWidth() < 768 {
            standardTextSize = 21
        } else {
            standardTextSize = 24
        }
        
    }

    var helveticaNeue: UIFont {
        return UIFont(name: "Helvetica Neue", size: standardTextSize)!
    }
    
    var helveticaNeueLight: UIFont {
        return UIFont(name: "HelveticaNeue-Light", size: standardTextSize)!
    }
    
}

/// Indicates network activity
func indicateNetworkActivity(){
    if !UIApplication.shared.isNetworkActivityIndicatorVisible {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
}

/// Stops indicating network activity
func stopNetworkActivity(){
    if UIApplication.shared.isNetworkActivityIndicatorVisible {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

/// Returns the screen height dynamically based on orientation
func screenWidth() -> CGFloat {
    return UIScreen.main.bounds.width
}

/// Returns the screen height dynamically based on orientation
func screenHeight() -> CGFloat {
    return UIScreen.main.bounds.height
}


    
