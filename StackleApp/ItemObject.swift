//
//  ItemObject.swift
//  StackleApp
//
//  Created by Stackle005 on 9/13/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import UIKit

class ItemObject: ReportsObject {
    
    fileprivate var _item_groupe_id: String!
    fileprivate var _item_id: String!
    fileprivate var _item_type: String!
    fileprivate var _image: UIImage!

    var itemGroupeId : String {
        return _item_groupe_id
    }
    var ItemId : String {
        return _item_id
    }
    var ItemType: String {
        return _item_type
    }
    
    init(ItemGroupeId: String, ItemId: String, ItemType: String, age: Int, businessId: String,  reporterUid : String, storeId: String, tags: String, text: String, type: reportTypes, imageId: String, url: String, reportKey: String) {
        super.init(age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, imageId: imageId, url: url, reportKey: reportKey)
        
        self._item_groupe_id = ItemGroupeId
        self._item_id = ItemId
        self._item_type = ItemType
    }
    func setImage(_ image: UIImage) {
        _image = image
    }
}
