//
//  CommentObject.swift
//  StackleApp
//
//  Created by Stackle005 on 9/13/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import UIKit

class CommentObject: ReportsObject {
    
    fileprivate var _reportee_uid: String!
    fileprivate var _review_id: String!
    fileprivate var _image: UIImage!
    
    var reporteeUid : String {
        return _reportee_uid
    }
    var reviewId : String {
        return _review_id
    }
    
    init(reporteeUid: String, reviewId: String, age: Int, businessId: String,  reporterUid : String, storeId: String, tags: String, text: String, type: reportTypes, imageId: String, url: String, reportKey: String) {
        super.init(age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, imageId: imageId, url: url, reportKey: reportKey)
        
        self._reportee_uid = reporteeUid
        self._review_id = reviewId
    }
    func setImage(_ image: UIImage) {
        _image = image
    }
}
