//
//  textViewWithToolbar.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/01.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class textViewWithToolbar: UITextView {

    var toolbarDelegate: customDelegate?
    
    override func awakeFromNib() {
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let sizeButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(textViewWithToolbar.resignTextviewResponder))
        
        toolbar.setItems([sizeButton, doneButton], animated: true)
        self.inputAccessoryView = toolbar
        
    }
    
    func resignTextviewResponder(){
        self.resignFirstResponder()
        toolbarDelegate?.doneButton!()
    }

}
