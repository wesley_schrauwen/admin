//
//  alerts.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/28.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class alerts {
    
    var viewController: UIViewController!
    var imagePicker: UIImagePickerController!
    
    enum alertTitles: String {
        case none = ""
    }
    
    enum alertMessages: String {
        case remove = "Remove this item?"
        case update = "Update this value?"
        case editOwner = "Enter the new owners email address to add / change the existing owner"
        case addMember = "Enter the new members email address."
        case addCategory = "Add this category?"
        case none = ""
    }
    
    enum actionTitles: String {
        case remove = "Remove"
        case update = "Update"
        case submit = "Submit"
        case add = "Add"
        case none = ""
    }
    
    /**
     Presents an alert used to either edit, remove an item
    */
    
    func editItemAlert(_ viewController: UIViewController, update: @escaping () -> Void, remove: @escaping () -> Void){
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let updateAction = UIAlertAction(title: "Update", style: .default, handler: { _ in
            update()
        })
        
        let removeAction = UIAlertAction(title: "Remove", style: .destructive, handler: { _ in
            remove()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        controller.addAction(updateAction)
        controller.addAction(removeAction)
        controller.addAction(cancelAction)
        
        viewController.present(controller, animated: true, completion: nil)
    }
    
    /**
     Presents a caution alert to the user before taking an action.
     
     Text may be customised using the provided enum values.
    */
    
    func cautionAlert(_ viewController: UIViewController, controllerMessage: alertMessages = .update, actionTitle: actionTitles = .update, callback: @escaping () -> Void){
        
        let controller = UIAlertController(title: "", message: controllerMessage.rawValue, preferredStyle: .alert)
        
        let actionOne = UIAlertAction(title: actionTitle.rawValue, style: .destructive, handler: { _ in
            callback()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        controller.addAction(actionOne)
        controller.addAction(cancelAction)
        
        viewController.present(controller, animated: true, completion: nil)
        
    }
    
    /**
     This alert is presented with a textfield.
     
     - NOTE: If used in adding or changing the business owner. The presentingVC needs to know if the business has an owner or not and select which call to make accordingly. Data WILL be corrupted if that check is not done and performed correctly.
    */
    
    func textFieldAlert(_ viewController: UIViewController, message: alertMessages = .none, actionTitle: actionTitles = .submit, callback: @escaping (String) -> Void){
        
        let controller = UIAlertController(title: "", message: message.rawValue, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let submitAction = UIAlertAction(title: actionTitle.rawValue, style: .destructive, handler: { _ in
            
            let textfield = controller.textFields![0]
            
            if let text = textfield.text {
                callback(text)
            }
            
            
        })
        
        controller.addTextField(configurationHandler: { textfield in
        
            textfield.placeholder = "Email Address"
            
        })
        
        controller.addAction(cancelAction)
        controller.addAction(submitAction)
        
        viewController.present(controller, animated: true, completion: nil)
        
    }
    
    /**
     Displays an image picker action sheet safely to the user depending on camera availability.
     Note: Not safe for tablet use does not take into account anchor and pivot points.
    */
    func imagePickerActionSheet(_ viewController: UIViewController, imagePicker: inout UIImagePickerController){
        
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        self.viewController = viewController
        self.imagePicker = imagePicker
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.imagePicker.sourceType = .camera
            self.viewController.present(self.imagePicker, animated: true, completion: nil)
        })
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.imagePicker.sourceType = .photoLibrary
            self.viewController.present(self.imagePicker, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        if UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.front) ||  UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.rear){
            controller.addAction(cameraAction)
        }
        
        controller.addAction(galleryAction)
        controller.addAction(cancelAction)
        
        viewController.present(controller, animated: true, completion: nil)
    }
    
    /**
     Displays the action sheet for the main page
    */
    func basicActionSheet(_ viewController: UIViewController, callback: @escaping () -> Void) {
        
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actionOne = UIAlertAction(title: "Add Unowned Business", style: .default, handler: { _ in
            callback()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        controller.addAction(actionOne)
        controller.addAction(cancelAction)
        
        viewController.present(controller, animated: true, completion: nil)
        
    }
    
    /**
     The first alert shown for resolution.
     
     Provides three options: Resolve, Ignore and Contact.
    */
    func resolutionAlert(_ viewController: UIViewController){
        
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let resolveAction = UIAlertAction(title: "Resolve", style: .default, handler: { _ in
            viewController.performSegue(withIdentifier: "ResolveReportSegue", sender: nil)
        })
        
        let ignoreAction = UIAlertAction(title: "Ignore", style: .default, handler: { _ in
            viewController.performSegue(withIdentifier: "IgnoreReportSegue", sender: nil)
        })
        let contactAction = UIAlertAction(title: "Contact", style: .default, handler: { _ in
            viewController.performSegue(withIdentifier: "ContactReportSegue", sender: nil)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        controller.addAction(contactAction)
        controller.addAction(ignoreAction)
        controller.addAction(resolveAction)
        controller.addAction(cancelAction)
        
        viewController.present(controller, animated: true, completion: nil)
        
    }
    
    /**
     The final alert shown for resolving a report.
     
     Provides two options: Remove and Resolve.
     
     - Remove: Sends a callback allowing for custom execution
     
     - Repace: Presents an image picker if the report type is an image.
     
    */
    
    func resolveAlert(_ viewController: UIViewController, reportType: reportTypes, imagePicker: inout UIImagePickerController, callback: @escaping () -> Void){
        
        let controller = UIAlertController(title: "Choose Resolution", message: "What action would you like to take?", preferredStyle: .actionSheet)
        
        self.viewController = viewController
        self.imagePicker = imagePicker
        
        let removeAction = UIAlertAction(title: "Remove", style: .default, handler: { _ in
            
            callback()
            
        })
        
        // only used with image reports
        let replaceAction = UIAlertAction(title: "Replace", style: .default, handler: { _ in
            self.imagePicker.sourceType = .photoLibrary
            self.viewController.present(self.imagePicker, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        controller.addAction(removeAction)
        
        if reportType == reportTypes.image {
            controller.addAction(replaceAction)
        }
        
        controller.addAction(cancelAction)
        
        viewController.present(controller, animated: true, completion: nil)
        
    }
    
}
