//
//  itemManifestTVCell.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class itemManifestTVCell: UITableViewCell {
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellDetailTextView: UITextView!
    @IBOutlet weak var cellLikesLabel: UILabel!
    @IBOutlet weak var cellPriceLabel: UILabel!
    @IBOutlet weak var cellTitleLabel: UILabel!
    
    fileprivate var indexPath: IndexPath!
    
    func setupCell(_ cellData: itemManifestObject, indexPath: IndexPath, callback: @escaping (UIImage) -> Void){
        
        self.indexPath = indexPath
        cellDetailTextView.text = cellData.detail
        cellLikesLabel.text = cellData.likes
        cellPriceLabel.text = cellData.price
        cellTitleLabel.text = cellData.name
        
        if let image = cellData.image {
            cellImage.image = image
        } else {
            
            NetworkUtilities.sharedFirebaseInstance.downloadImage(cellData.url, callback: { image in
            
                if self.indexPath == indexPath {
                    self.cellImage.image = image
                }
                
                callback(image)
            
            })
            
        }
        
        
    }
}
