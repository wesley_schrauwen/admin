//
//  AddUnownedBusinessVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/28.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class AddUnownedBusinessVC: ParentVC, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var homeImageView: UIButton!
    @IBOutlet weak var logoView: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var sundayCloseTextfield: UITextField!
    @IBOutlet weak var sundayOpenTextfield: UITextField!
    @IBOutlet weak var saturdayCLoseTextfield: UITextField!
    @IBOutlet weak var saturdayOpenTextfield: UITextField!
    @IBOutlet weak var fridayCloseTextfield: UITextField!
    @IBOutlet weak var fridayOpenTextfield: UITextField!
    @IBOutlet weak var thursdayCloseTextfield: UITextField!
    @IBOutlet weak var thursdayOpenTextfield: UITextField!
    @IBOutlet weak var wednesdayCloseTextfield: UITextField!
    @IBOutlet weak var wednesdayOpenTextfield: UITextField!
    @IBOutlet weak var tuesdayCloseTextfield: UITextField!
    @IBOutlet weak var tuesdayOpenTextfield: UITextField!
    @IBOutlet weak var mondayCloseTextfield: UITextField!
    @IBOutlet weak var mondayOpenTextfield: UITextField!
    @IBOutlet weak var latlongLabel: UIButton!
    @IBOutlet weak var locationNameTextfield: UITextField!
    @IBOutlet weak var emailAddressTextfield: UITextField!
    @IBOutlet weak var phoneNumberTextfield: UITextField!
    @IBOutlet weak var businessNameTextfield: UITextField!
    
    fileprivate var imagePicker = UIImagePickerController()
    fileprivate var isSettingLogo: Bool = false
    fileprivate var lat: Double?
    fileprivate var long: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        businessNameTextfield.delegate = self
        phoneNumberTextfield.delegate = self
        emailAddressTextfield.delegate = self
        locationNameTextfield.delegate = self
        
        mondayOpenTextfield.delegate = self
        tuesdayOpenTextfield.delegate = self
        wednesdayOpenTextfield.delegate = self
        thursdayOpenTextfield.delegate = self
        fridayOpenTextfield.delegate = self
        saturdayOpenTextfield.delegate = self
        sundayOpenTextfield.delegate = self
        
        mondayCloseTextfield.delegate = self
        tuesdayCloseTextfield.delegate = self
        wednesdayCloseTextfield.delegate = self
        thursdayCloseTextfield.delegate = self
        fridayCloseTextfield.delegate = self
        saturdayCLoseTextfield.delegate = self
        sundayCloseTextfield.delegate = self
        
        imagePicker.delegate = self
        imagePicker.sourceType =  .photoLibrary
        
        logoView.setTitle("Set Logo", for: UIControlState())
        logoView.setTitleColor(UIColor.black, for: UIControlState())
        homeImageView.setTitle("Set Home Image", for: UIControlState())
        homeImageView.setTitleColor(UIColor.black, for: UIControlState())
    }
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ParentVC {
            vc.delegate = self
        }
        
        if segue.identifier == "setBusinessHoursSegue" {
            let vc = segue.destination as! setBusinessHoursVC
            vc.hoursLabelText = sender as! String
        }
    }
    
    // MARK: Delegate
    
    func coordsSet(_ lat: Double, long: Double) {
        self.lat = lat
        self.long = long
        
        self.latlongLabel.setTitle("Lat / Long Set at ~ \(Int(lat)), \(Int(long))", for: UIControlState())
    }
    
    func businessHoursSet(_ type: String, open: String, close: String) {
        
        
        switch type.lowercased() {
        case "all":
            mondayOpenTextfield.text = open
            tuesdayOpenTextfield.text = open
            wednesdayOpenTextfield.text = open
            thursdayOpenTextfield.text = open
            fridayOpenTextfield.text = open
            saturdayOpenTextfield.text = open
            sundayOpenTextfield.text = open
            
            mondayCloseTextfield.text = close
            tuesdayCloseTextfield.text = close
            wednesdayCloseTextfield.text = close
            thursdayCloseTextfield.text = close
            fridayCloseTextfield.text = close
            saturdayCLoseTextfield.text = close
            sundayCloseTextfield.text = close
            break
        case "monday":
            mondayOpenTextfield.text = open
            mondayCloseTextfield.text = close

            break
        case "tuesday":
            tuesdayOpenTextfield.text = open
            tuesdayCloseTextfield.text = close

            break
        case "wednesday":
            wednesdayOpenTextfield.text = open
            wednesdayCloseTextfield.text = close

            break
        case "thursday":
            thursdayOpenTextfield.text = open
            thursdayCloseTextfield.text = close

            break
        case "friday":
            fridayOpenTextfield.text = open
            fridayCloseTextfield.text = close

            break
        case "saturday":
            saturdayOpenTextfield.text = open
            saturdayCLoseTextfield.text = close

            break
        case "sunday":
            sundayOpenTextfield.text = open
            sundayCloseTextfield.text = close

            break
        default:
            break
        }
        
    }
    
    // MARK: Textfield
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return textField == businessNameTextfield || textField == phoneNumberTextfield || textField == emailAddressTextfield || textField == locationNameTextfield
        
    }
    
    // MARK: Image Picker
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        isSettingLogo ? {
            self.logoView.setImage(image, for: UIControlState())
            self.logoView.setTitleColor(UIColor.white, for: UIControlState())
            }() : {
                self.homeImageView.setImage(image, for: UIControlState())
                self.homeImageView.setTitleColor(UIColor.white, for: UIControlState())
            }()
        
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    // MARK: Action
    @IBAction func setLogoButton(_ sender: AnyObject) {
        
        isSettingLogo = true
        alerts().imagePickerActionSheet(self, imagePicker: &imagePicker)
        
    }
    
    @IBAction func cancelButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButton(_ sender: AnyObject) {
        
        if businessNameTextfield.text == "" || phoneNumberTextfield.text == "" || emailAddressTextfield.text == "" || locationNameTextfield.text == "" || mondayCloseTextfield.text == "" || mondayOpenTextfield.text == "" || lat == nil || long == nil || logoView.currentImage == nil || homeImageView.currentImage == nil {
            
            return
        } else {
            
            let obj = unownedBusinessObject()
            
            obj.name = businessNameTextfield.text
            obj.storePhone = phoneNumberTextfield.text
            obj.storeEmail = emailAddressTextfield.text
            obj.locationName = locationNameTextfield.text
            obj.lat = lat
            obj.long = long
            obj.logo = logoView.currentImage
            obj.homeImage = homeImageView.currentImage
            obj.mondayOpen = mondayOpenTextfield.text
            obj.mondayClose = mondayCloseTextfield.text
            obj.tuesdayOpen = tuesdayOpenTextfield.text
            obj.tuesdayClose = tuesdayCloseTextfield.text
            obj.wednesdayOpen = wednesdayOpenTextfield.text
            obj.wednesdayClose = wednesdayCloseTextfield.text
            obj.thursdayOpen = thursdayOpenTextfield.text
            obj.thursdayClose = thursdayCloseTextfield.text
            obj.fridayOpen = fridayOpenTextfield.text
            obj.fridayClose = fridayCloseTextfield.text
            obj.saturdayOpen = saturdayOpenTextfield.text
            obj.saturdayClose = saturdayCLoseTextfield.text
            obj.sundayOpen = sundayOpenTextfield.text
            obj.sundayClose = sundayCloseTextfield.text
            
            self.view.alpha = 0.75
            activityIndicator.startAnimating()
            
            network.createdUnownedBusiness(obj, homeImageProgress: { progress in
                
                if progress == 1 {
                    
                    self.homeImageView.layer.borderColor = UIColor.green.cgColor
                    self.homeImageView.layer.borderWidth = 2
                    
                } else if progress >= 0 {
                    
                    self.homeImageView.imageView?.alpha = CGFloat(progress)
                    
                } else if progress == -1 {
                    
                }
                
                }, logoProgress: { progress in
                
                    if progress == 1 {
                        
                        self.logoView.layer.borderColor = UIColor.green.cgColor
                        self.logoView.layer.borderWidth = 2
                        
                    } else if progress >= 0 {
                        
                        self.logoView.imageView?.alpha = CGFloat(progress)
                        
                    } else if progress == -1 {
                        
                    }
                    
                }, success: { complete in
            
                    self.view.alpha = 1
                    self.activityIndicator.stopAnimating()
                    
                    if complete {
                        toast().basic(self, message: "Success!")
                    } else {
                        toast().basic(self, message: "Create failed")
                    }
                    
                    
                    
            })
            
        }
        
        
    }
    
    @IBAction func setHomeImageButton(_ sender: AnyObject) {

        isSettingLogo = false
        alerts().imagePickerActionSheet(self, imagePicker: &imagePicker)
    
    }
    
    @IBAction func setLatLongButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "getCoordsSegue", sender: nil)
    }
    
    @IBAction func setBusinessHoursButton(_ sender: AnyObject) {
        
        performSegue(withIdentifier: "setBusinessHoursSegue", sender: "all")
        
    }
    
    @IBAction func setSpecificBusinessHour(_ sender: UIButton) {
        performSegue(withIdentifier: "setBusinessHoursSegue", sender: sender.titleLabel!.text!.lowercased())

        
    }
    
}
