//
//  BusinessObject.swift
//  StackleApp
//
//  Created by Stackle005 on 9/9/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import Firebase
import UIKit

class BusinessObject: ReportsObject {
  
    fileprivate var _image_id: String!
    fileprivate var _image: UIImage!
    
    init(imageId: String, age: Int, businessId: String, reporterUid : String, storeId: String, tags: String, text: String, type: reportTypes, url: String, reportKey: String) {
        super.init(age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, imageId: imageId, url: url, reportKey: reportKey)
    
        self._image_id = imageId
    }
    
    func setImage(_ image: UIImage) {
             _image = image
    }
}
