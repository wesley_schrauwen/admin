//
//  unownedParentVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/01.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class unownedParentVC: ParentVC {
    
    var data: unownedBusinessObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard data != nil || data.businessID != nil else {
            self.navigationController?.popViewController(animated: true)
            return
        }
    }
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: nil)
        
        if let vc = segue.destination as? unownedParentVC {
            vc.data = self.data
        }
        
    }

}
