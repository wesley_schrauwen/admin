//
//  ImageObject.swift
//  StackleApp
//
//  Created by Stackle005 on 8/30/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import UIKit

class ImageObject: ReportsObject {
    
    fileprivate var _image_id: String!
    fileprivate var _is_Official : Bool!
    fileprivate var _reportee_uid : String!
    fileprivate var _name: String!
    fileprivate var _image: UIImage!

    var isOfficial : Bool {
        return _is_Official
    }
    var reporteeUid : String {
        return _reportee_uid
    }
    var name: String {
        return _name
    }
    
    init(imageName: String, imageId: String, isOfficial: Bool, reporteeUid: String, imageURL: String = "", age: Int, businessId: String,  reporterUid : String, storeId: String, tags: String, text: String, type: reportTypes, url: String, reportKey: String) {
        super.init(age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, imageId: imageId, url: url, reportKey: reportKey)
        
        self._name = imageName
        self._image_id = imageId
        self._is_Official = isOfficial
        self._reportee_uid = reporteeUid
    }
    func setImage(_ image: UIImage) {
        _image = image
    }
}
