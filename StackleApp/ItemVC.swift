//
//  ItemVC.swift
//  StackleApp
//
//  Created by Stackle005 on 9/8/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class ItemVC: ResolveReportVC {

    @IBOutlet weak var Lbl1: UILabel!
    @IBOutlet weak var Lbl2: UILabel!
    @IBOutlet weak var Lbl3: UILabel!
    @IBOutlet weak var Lbl4: UILabel!
    @IBOutlet weak var businessId: UILabel!
    @IBOutlet weak var itemGroupeId: UILabel!
    @IBOutlet weak var ItemType: UILabel!
    @IBOutlet weak var ItemId: UILabel!
    @IBOutlet weak var tags: UILabel!
    @IBOutlet weak var text: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tags.text = report.tags
        text.text = report.text
    }
    
    @IBAction func resolutionBtnClicked(_ sender: AnyObject) {
        alerts().resolutionAlert(self)
    }
}
