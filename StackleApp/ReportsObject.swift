//
//  ReportsObject.swift
//  StackleApp
//
//  Created by Stackle005 on 8/30/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import UIKit

class ReportsObject {
    
    fileprivate var _age : Int!
    fileprivate var _business_id : String!
    fileprivate var _reporter_uid : String!
    fileprivate var _store_id : String!
    fileprivate var _tags : String!
    fileprivate var _text : String!
    fileprivate var _type : reportTypes!
    fileprivate var _imageId : String!
    fileprivate var _url: String!
    fileprivate var _report_key: String!
    
    var age : Int {
        return _age
    }
    var businessId : String {
        return _business_id
    }
    var reporterUid : String {
        return _reporter_uid
    }
    var storeId : String {
        return _store_id
    }
    var tags : String {
        return _tags
    }
    var text : String {
        return _text
    }
    var type : reportTypes {
        return _type
    }
    var imageId : String {
        return _imageId
    }
    var url : String {
        return _url
    }
    var reportKey : String {
        return _report_key
    }
    
    init(age: Int, businessId: String, reporterUid: String, storeId: String, tags: String, text: String, type: reportTypes, imageId: String, url: String, reportKey: String ) {
        
        self._age = age
        self._business_id = businessId
        self._reporter_uid = reporterUid
        self._store_id = storeId
        self._tags = tags
        self._text = text
        self._type = type
        self._imageId = imageId
        self._url = url
        self._report_key = reportKey
   
        if imageId == "" {
            String().logoImageURL(businessId, callback: { url in
                self._url = url
            })
        } else if businessId != "" {
            _url = String().officialImageURL(businessId, storeID: storeId, imageID: imageId)
        } else {
            _url = String().reviewImageURL(businessId, storeID: storeId, imageID: imageId)
        }
    
 //   func setImage(image: UIImage) {
   //     _image = image
 //   }
    }
}





