//
//  manageStoreVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/03.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class manageStoreVC: storeParentVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var geoAddressLabel: UIButton!
    @IBOutlet weak var homeImageView: UIButton!
    @IBOutlet weak var locationNameTextfield: UITextField!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var reviewsLabel: UILabel!
    
    fileprivate var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        locationNameTextfield.delegate = self
        
        network.downloadStoreDashboard(storeData, callback: { data in
            
            self.storeData = data
            self.geoAddressLabel.setTitle("Geo: \(data.geoAddress)", for: UIControlState())
            self.homeImageView.setImageForURL(data.url)
            self.locationNameTextfield.text = data.locationName
            self.likesLabel.text = "Likes: \(data.likes)"
            self.ratingLabel.text = "Rating: \(data.rating)"
            self.reviewsLabel.text = "Reviews: \(data.reviews)"
            
            }, failed: {
                toast().VCInitFail(self)
        })
        
    }
    
    
    // MARK: Textfield
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard textField.text != nil else {
            return
        }
        
        alerts().cautionAlert(self, callback: {
            self.storeData.updateLocationName(textField.text!)
            self.network.updateLocation(self.storeData, updateLocationName: true, callback: { success in
                toast().success(self, success: success)
            })
        })
        
    }
    
    // MARK: Image Picker
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        self.dismiss(animated: true, completion: nil)
        storeData.createURL()
        self.homeImageView.setImage(image, for: UIControlState())
        
        network.updateHomeImage(storeData, image: image, progress: { progress in
            
            self.homeImageView.alpha = CGFloat(progress)
            self.homeImageView.isUserInteractionEnabled = false
            
            }, success: { success in
        
                toast().success(self, success: success)
                self.homeImageView.isUserInteractionEnabled = true
                
        })
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Delegate
    
    func coordsSet(_ lat: Double, long: Double) {
        
        storeData.updateCoords(lat, long: long)
        geoAddressLabel.setTitle("~ \(Int(lat)) : \(Int(long))", for: UIControlState())
        
        network.updateLocation(storeData, updateCoords: true, callback: { success in
            toast().success(self, success: success)
        })
    }
    
    // MARK: Action
    
    @IBAction func setImagesButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "addImagesSegue", sender: nil)
    }
    
    @IBAction func setContactDetailsButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "contactDetailsSegue", sender: nil)
    }
    
    @IBAction func setHomeImageButton(_ sender: AnyObject) {
        alerts().imagePickerActionSheet(self, imagePicker: &imagePicker)
    }

    @IBAction func setGeoAddressButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "getCoordsSegue", sender: nil)
    }
}
