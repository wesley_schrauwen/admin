//
//  oneLabelTVCell.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/02.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class oneLabelTVCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    func setupCell(_ cellData: storeParent){
        label.text = cellData.locationName
    }
    
}
