//
//  homeVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/28.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class MainVC: ParentVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate let locationManager = CLLocationManager()
    fileprivate var verificationRequests = [VerificationObject]()
    fileprivate var businessRequests = [unownedBusinessObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = CLLocationAccuracy(100)
        locationManager.distanceFilter = CLLocationDistance(50)
        
        getUserLocation()
        
        network.downloadDashboard(verificationRequests, verificationCallback: { object in
            
            if !self.verificationRequests.contains(where: { $0.businessID == object.businessID }) {
                self.verificationRequests.append(object)
                self.collectionView.insertItems(at: [IndexPath(row: self.verificationRequests.count - 1, section: 1)])
            }
            
            }, existingUnownedBusinesses: businessRequests, businessCallback: { object in
                
                if !self.businessRequests.contains(where: { $0.businessID == object.businessID }) {
                    self.businessRequests.append(object)
                    self.collectionView.insertItems(at: [IndexPath(item: self.businessRequests.count - 1, section: 2)])
                }
        })
        
    }
    
    // MARK: Core Location
    
    fileprivate func getUserLocation() {
    
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            if CLLocationManager.locationServicesEnabled() {
                locationManager.requestLocation()
            }
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //need to implement a check for the time stamp of the location. If the location is older than 30 mins then request a new location.
        locationManager.desiredAccuracy = CLLocationAccuracy(50)
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        debugPrint(error)
    }

    
    
    // MARK: Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 5
        case 1:
            return verificationRequests.count
        case 2:
            return businessRequests.count
        default: return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "oneImageOneLabelCVCell", for: indexPath) as? oneImageOneLabelCVCell {
                switch indexPath.row {
                case 0: cell.setupCell(reportTypes.business); break
                case 1: cell.setupCell(reportTypes.comment); break
                case 2: cell.setupCell(reportTypes.image); break
                case 3: cell.setupCell(reportTypes.item); break
                case 4: cell.setupCell(reportTypes.reply); break
                case 5: cell.setupCell(reportTypes.review); break
                default: break
                }
                return cell
            }
        case 1:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "oneImageOneLabelCVCell", for: indexPath) as? oneImageOneLabelCVCell {
                
                cell.setupCell(verificationRequests[indexPath.row], index: indexPath, callback: { image in
                    self.verificationRequests[indexPath.row].setLogo(image)
                })
                
                return cell
                
            }
        case 2:
            
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "oneImageOneLabelCVCell", for: indexPath) as? oneImageOneLabelCVCell {
                
                cell.setupCell(businessRequests[indexPath.row], index: indexPath, callback: { image in
                
                    self.businessRequests[indexPath.row].logo = image
                    
                })
                
                return cell
            }
            
        default: return UICollectionViewCell()
        }
        
        return UICollectionViewCell()
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "reusableHeaderView", for: indexPath) as? HeaderReusableView {
            switch indexPath.section {
            case 0:
                view.setupView("Reports")
                break
            case 1:
                view.setupView("Verification")
                break
            case 2:
                view.setupView("Unowned Businesses")
                break
            default: break
            }
            return view
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            
            if let _ = collectionView.cellForItem(at: indexPath) as? oneImageOneLabelCVCell {
                performSegue(withIdentifier: "reportSegue", sender: indexPath.row)
            }
            
            break
        case 1:
            
                self.performSegue(withIdentifier: "verificationSegue", sender: verificationRequests[indexPath.row])
            
            break
        case 2:
            
            self.performSegue(withIdentifier: "unownedSegue", sender: businessRequests[indexPath.row])
            
            break
        default: break
        }
    }
    
    
    
    // MARK: Action
    
    @IBAction func addButton(_ sender: AnyObject) {
        
        alerts().basicActionSheet(self, callback: {
            self.performSegue(withIdentifier: "AddUnownedSegue", sender: nil)
        })
    }
    
    
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "reportSegue"?:
            
            let vc = segue.destination as! ReportMainVC
            
            vc.reportToShow = { _ in
                switch sender as! Int {
                case 0: return reportTypes.business
                case 1: return reportTypes.comment
                case 2: return reportTypes.image
                case 3: return reportTypes.item
                case 4: return reportTypes.reply
                case 5: return reportTypes.review
                default: return reportTypes.none
                }
            }()
            
            break
        case "unownedSegue"?:
            let vc = segue.destination as! ManageUnownedVC
            vc.isUnowned = true
            vc.data = sender as! unownedBusinessObject
            break
        case "AddUnownedSegue"?:
            //Nothing to put in here yet.
            break
        case "verificationSegue"?:
            let vc = segue.destination as! VerificationVC
            vc.verification = sender as! VerificationObject
            break
        default: break
        }
        
    }
}
