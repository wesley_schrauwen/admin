//
//  galleryVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/03.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class galleryVC: storeParentVC, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var images = [galleryObject]()
    fileprivate var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        imagePicker.delegate = self
        downloadImages()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        FIREBASE_DATABASE.child("businesses/\(storeData.businessID)/stores/\(storeData.id)/images/official").removeAllObservers()
    }
    
    // MARK: Image Picker
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        self.dismiss(animated: true, completion: nil)
        
        network.createImage(storeData, image: image, callback: { success in
        
            toast().success(self, success: success)
            
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: Action
    
    @IBAction func addImageButton(_ sender: AnyObject) {
        alerts().imagePickerActionSheet(self, imagePicker: &imagePicker)
    }
    
    // MARK: Collection View
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return images.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCVCell", for: indexPath) as? imageCVCell {
            
            cell.setupCell(images[indexPath.row], indexPath: indexPath, callback: { image in
                
                self.images[indexPath.row].setImage(image)
            
            })
            
            return cell
        }
        
        return UICollectionViewCell()
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        alerts().cautionAlert(self, controllerMessage: alerts.alertMessages.remove, actionTitle: alerts.actionTitles.remove, callback: {
        
            self.network.removeGalleryItem(self.images[indexPath.row], callback: { success in
            
                self.collectionView.deleteItems(at: [IndexPath(item: self.images.count - 1, section: 0)])
                self.images.remove(at: indexPath.row)
                toast().success(self, success: success)
            
            })
            
        })
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.row - 5 > images.count && images.count >= 10 && images.count % 10 == 0{
            downloadImages()
        }
        
    }
    
    // MARK: Convenience
    
    fileprivate func downloadImages(){
        network.downloadGallery(self.storeData, existingData: images, callback: { object in
            
            if !self.images.contains(where: { $0.imageID == object.imageID }) {
                self.images.append(object)
                self.collectionView.insertItems(at: [IndexPath(item: self.images.count - 1, section: 0)])
            }
            
        })
    }

}
