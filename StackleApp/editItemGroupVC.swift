//
//  editItemGroupVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class editItemGroupVC: itemParentVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var titleTextfield: UITextField!
    @IBOutlet weak var imageView: UIButton!
    
    var itemGroup: itemGroupObject?
    fileprivate var isEditingData = false
    fileprivate var updateImage = false
    fileprivate var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        titleTextfield.delegate = self

        if let itemGroup = itemGroup {
            titleTextfield.text = itemGroup.name
            isEditingData = true
            
            if let logo = itemGroup.logo {
                imageView.setImage(logo, for: UIControlState())
            } else {
                
                network.downloadImage(itemGroup.url, callback: { image in
                
                    itemGroup.setImage(image)
                    self.imageView.setImage(image, for: UIControlState())
                
                })
                
            }
        }
        
    }
    
    // MARK: Textfield
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    // MARK: Image Picker
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        self.dismiss(animated: true, completion: nil)
        
        if isEditingData {
            itemGroup?.setImage(image)
        }
        updateImage = true
        imageView.setImage(image, for: UIControlState())
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Action
    
    @IBAction func setImageButton(_ sender: AnyObject) {
        alerts().imagePickerActionSheet(self, imagePicker: &imagePicker)
    }
    
    @IBAction func doneButton(_ sender: AnyObject) {
        
        guard titleTextfield.text != nil || titleTextfield.text != "" || imageView.currentImage != nil else {
            return
        }
        
        titleTextfield.resignFirstResponder()
        
        if !isEditingData {
            itemGroup = itemGroupObject(itemGroupID: FIREBASE_DATABASE.childByAutoId().key, businessID: data.businessID!, name: titleTextfield.text!, url: "placeholder.png", type: itemType)
            itemGroup!.updateImage(imageView.currentImage!)
        } else {
            itemGroup!.setName(titleTextfield.text!)
        }
        
        self.network.updateItemGroup(itemGroup!, updateImage: updateImage, progress: { val in
            
            self.imageView.alpha = CGFloat(val)
            self.imageView.isUserInteractionEnabled = false
            
            }, callback: { success in
                toast().success(self, success: success)
        })
        
    }

}
