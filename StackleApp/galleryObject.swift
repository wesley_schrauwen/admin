//
//  galleryObject.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/03.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class galleryObject {
    
    fileprivate var _image: UIImage?
    fileprivate var _url: String!
    fileprivate var _imageID: String!
    fileprivate var _businessID: String!
    fileprivate var _storeID: String!
    fileprivate var _created: Int!
    
    var created: Int {
        return _created
    }
    
    var imageID: String {
        return _imageID
    }
    
    var businessID: String {
        return _businessID
    }
    
    var storeID: String {
        return _storeID
    }
    
    var image: UIImage? {
        return _image
    }
    
    var url: String {
        return _url
    }
    
    init(businessID: String, storeID: String, imageID: String, url: String, created: Int){
        _url = url
        _created = created
        _businessID = businessID
        _storeID = storeID
        _imageID = imageID
    }
    
    func setImage(_ image: UIImage){
        _image = image
    }
    
}
