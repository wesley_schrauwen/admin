//
//  editItemManifestVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class editItemManifestVC: itemParentVC, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {

    @IBOutlet weak var imageView: UIButton!
    @IBOutlet weak var wordCountLabel: UILabel!
    @IBOutlet weak var detailTextView: UITextView!
    @IBOutlet weak var priceTextfield: UITextField!
    @IBOutlet weak var titleTextfield: UITextField!
    
    var item: itemManifestObject?
    var group: itemGroupObject!
    fileprivate var isEditingData = false
    fileprivate var imagePicker = UIImagePickerController()
    fileprivate var updateImage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        
        detailTextView.delegate = self
        priceTextfield.delegate = self
        titleTextfield.delegate = self
        
        if let item = item {
            
            isEditingData = true
            wordCountLabel.text = "\(item.detail.characters.count)"
            detailTextView.text = item.detail
            priceTextfield.text = item.price
            titleTextfield.text = item.name
            
            if let image = item.image {
                self.imageView.setImage(image, for: UIControlState())
            } else {
                network.downloadImage(item.url, callback: { image in
                    self.imageView.setImage(image, for: UIControlState())
                    self.item!.setImage(image)
                })
            }
            
        }
        
    }
    
    // MARK: Textfield
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return textField == priceTextfield ? string.isValidTextFieldNumber : true
        
    }
    
    // MARK: Textview
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "item description..."
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "item description..." {
            textView.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let count = textView.text.characters.count + text.characters.count
        
        if count < 200 {
            wordCountLabel.text = "\(count)"
            return true
        } else {
            return false
        }
        
    }
    
    // MARK: Image Picker

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        self.dismiss(animated: true, completion: nil)
        
        if isEditingData {
            item!.updateImage(image)
        }
        self.imageView.setImage(image, for: UIControlState())
        
        updateImage = true
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Action
    
    @IBAction func editImageButton(_ sender: AnyObject) {
        alerts().imagePickerActionSheet(self, imagePicker: &imagePicker)
    }
    @IBAction func doneButton(_ sender: AnyObject) {
        
        guard imageView.currentImage != nil || titleTextfield.text != nil || titleTextfield.text != "" else {
            return
        }
        
        if priceTextfield.text == nil {
            priceTextfield.text = ""
        }
        
        if detailTextView.text == "item description..." {
            detailTextView.text = ""
        }
        
        if !isEditingData {
            item = itemManifestObject(id: FIREBASE_DATABASE.childByAutoId().key, groupID: group.id, businessID: group.businessID, name: titleTextfield.text!, price: priceTextfield.text!, detail: detailTextView.text, likes: "0", url: "placeholder.png", type: group.type)
            item!.updateImage(imageView.currentImage!)
        } else {
            item!.update(titleTextfield.text!, price: priceTextfield.text!, detail: detailTextView.text)
        }
        
        network.updateItemManifest(item!, updateImage: updateImage, progress: { progress in
            self.imageView.alpha = CGFloat(progress)
            self.imageView.isUserInteractionEnabled = false
            }, callback: { success in
                
                toast().success(self, success: success)
                
        })
        
    }

}
