//
//  rawCategoryObject.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class rawCategoryObject {
    
    fileprivate var _id: String!
    fileprivate var _title: String!
    fileprivate var _subtitles: [String]!
    fileprivate var _businessID: String!
    
    var businessID: String {
        return _businessID
    }
    
    var id: String {
        return _id
    }
    
    var title: String {
        return _title
    }
    
    var subtitles: [String] {
        return _subtitles
    }
    
    init(id: String, businessID: String, title: String, subtitles: [String]){
        _id = id
        _businessID = businessID
        _title = title
        _subtitles = subtitles
    }
    
}
