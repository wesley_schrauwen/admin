//
//  storeParentVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/03.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class storeParentVC: unownedParentVC {
    
    var storeData: storeObject!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let vc = segue.destination as? storeParentVC {
            vc.storeData = self.storeData
        }
    }
    
}
