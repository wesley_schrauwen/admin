//
//  NetworkUtilities.swift
//  StackleApp
//
//  Created by Stackle005 on 8/31/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import Firebase
import UIKit
import Alamofire

/// The class responsible for all network related activities.
class NetworkUtilities {
    
    /// The singleton instance for the networkUtilities class
    static let sharedFirebaseInstance = NetworkUtilities()
    
    fileprivate var alamofireManager: Alamofire.SessionManager!
    
    init(){
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 30
        config.httpMaximumConnectionsPerHost = 10
        
        alamofireManager = Alamofire.SessionManager(configuration: config)
    }
    
    /// Returns a user ID if available. If not available will sign out the currently signed in account.
    func getUserID() -> String? {
        if let auth = FIRAuth.auth(), let user = auth.currentUser {
            return user.uid
        } else {
            do {
                try FIRAuth.auth()?.signOut()
                
            } catch let e {
                debugPrint(e)
            }
            return nil
        }
    }
    
    /// Returns content type of "image/jpg". Mostly used in putting images onto firebase storage
    func metaData() -> FIRStorageMetadata {
        let _metaData = FIRStorageMetadata()
        _metaData.contentType = "image/jpg"
        return _metaData
    }
    
    /// A convenience class for initialising a post request. Can handle an enum for destination.
    fileprivate func initPost(_ destination: requests, params: Dictionary <String, Any>, callBack: ((Any?) -> Void)? = nil, error: (((NSError)?) -> Void)? = nil){
    
        let startTime = Date().timeIntervalSinceReferenceDate
        
        indicateNetworkActivity()
        
        alamofireManager.request(_google_engine+destination.rawValue, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            
            stopNetworkActivity()
            let endTime = NSDate().timeIntervalSinceReferenceDate
            let _diff = (endTime - startTime) * 100
            let rounded: Float = Float(Int(_diff))
            
            print("\(destination)... \nROUND TRIP TIME: \(rounded / 100) seconds");
            print(response.result.error)
            
            if let value = response.result.value as? Dictionary<String, AnyObject>, let code = value["code"] as? Int {
                
                if code == 200 {
                    callBack?(response.result.value)
                }
                
                
            } else if response.result.error != nil {
                
                error?(response.result.error as (NSError)?)
                
            } else {
                
                callBack?(response.result.value)
            }
            
        })
        
    }
    
    
    /// Deletes a category from a business
    func deleteCategory(_ object: categoryObject, callback: @escaping (Bool) -> Void){
        
        initPost(requests.deleteCategory, params: [
            "business_id": object.businessID as AnyObject,
            "category_id": object.id as AnyObject,
            "subtitle": object.subtitle as AnyObject
            ], callBack: { _ in callback(true) }, error: { _ in callback (false) })
        
    }
    
    /// downloads all available categories that a business may have.
    func downloadRawCategories(_ businessID: String, callback: @escaping (rawCategoryObject) -> Void){
        
        FIREBASE_DATABASE.child("category_list/").queryOrdered(byChild: "is_available").queryEqual(toValue: true).observe(.childAdded, with: { snapshot in
        
            if snapshot.exists(), let data = snapshot.value as? Dictionary<String, AnyObject> {
                
                var subcategories = [String]()
                var title = ""
                
                if let _val = data["subcategories"] as? [String] {
                    subcategories = _val
                }
                
                if let _val = data["title"] as? String {
                    title = _val
                }
                
                callback(rawCategoryObject(id: snapshot.key, businessID: businessID, title: title, subtitles: subcategories))
                
            }
        
        })
        
    }
    
    /// Downloads the categories for a business
    func downloadCategories(_ businessID: String, callback: @escaping (categoryObject) -> Void){
        
        FIREBASE_DATABASE.child("businesses/\(businessID)/categories").observe(.childAdded, with: { snapshot in
        
            if snapshot.exists(), let data = snapshot.value as? Dictionary<String, AnyObject> {
                
                var title = ""
                var subtitle = ""
                
                if let _val = data["title"] as? String {
                    title = _val
                }
                if let _val = data["subtitle"] as? String {
                    subtitle = _val
                }
                
                callback(categoryObject(id: snapshot.key, businessID: businessID, title: title, subtitle: subtitle))
                
            }
        
        })
        
    }
    
    /// Updates the item manifest for a business
    func updateItemManifest(_ object: itemManifestObject, updateImage: Bool, progress: @escaping (Double) -> Void, callback: @escaping (Bool) -> Void){
        
        
        func updateDatabase(){
            initPost(requests.updateItem, params: [
                "business_id": object.businessID as AnyObject,
                "item_group_id": object.groupID as AnyObject,
                "title": object.name as AnyObject,
                "image_url": object.url as AnyObject,
                "item_type": object.type as AnyObject,
                "item_id": object.id as AnyObject,
                "price": object.price as AnyObject,
                "description": object.detail
                ], callBack: { _ in callback (true) }, error: { _ in callback (false) })
        }
        
        updateImage ? {
            
            self.updateImage(object.image!, url: object.url, progress: { _val in progress(_val) }, callback: { complete in
                
                complete ? updateDatabase() : callback (false)
                
            })
            
            }() : updateDatabase()
        
    }
    
    /// Deletes an item from its manifest
    func deleteItemManifest(_ object: itemManifestObject, callback: @escaping (Bool) -> Void){
        
        
        initPost(requests.deleteItem, params: [
            "business_id": object.businessID as AnyObject,
            "item_group_id": object.groupID as AnyObject,
            "item_type": object.type as AnyObject,
            "item_id": object.id as AnyObject
            ], callBack: { _ in
                callback(true)
            }, error: { _ in
                callback(false)
        })
        
    }
    
    /// Downloads all the items in a manifest for a businesses
    func downloadItemManifest(_ businessID: String, type: String, groupID: String, callback: @escaping (itemManifestObject) -> Void){
        
        FIREBASE_DATABASE.child("businesses/\(businessID)/\(type)_manifest/\(groupID)/").observe(.childAdded, with: { snapshot in
        
            if snapshot.exists(), let data = snapshot.value as? Dictionary <String, AnyObject> {
                
                var title = ""
                var price = ""
                var detail = ""
                let id = snapshot.key
                var likes = 0
                var url = ""
                
                
                if let _val = data["image"] as? String {
                    url = _val
                }
                if let _val = data["title"] as? String {
                    title = _val
                }
                
                if let _val = data["price"] as? String {
                    price = _val
                }
                if let _val = data["description"] as? String {
                    detail = _val
                }
                if let _val = data["likes"] as? Int {
                    likes = _val
                }
                
                callback(itemManifestObject(id: id, groupID: groupID, businessID: businessID, name: title, price: price, detail: detail, likes: "\(likes)", url: url, type: type))
                
            }
        
        
        })
        
    }
    
    /// Removes an item group from a particular business.
    func removeItemGroup(_ object: itemGroupObject, callback: @escaping (Bool) -> Void){
        initPost(requests.deleteItemGroup, params: [
            "business_id": object.businessID as AnyObject,
            "item_group_id": object.id as AnyObject,
            "item_type": object.type as AnyObject
            ], callBack: { _ in
            
                callback(true)
                
            }, error: { _ in
                
                callback(false)
        })
    }
    
    /// Removes a member from the unowned business
    func removeMember(_ businessID: String, memberEmail: String){
        
        FIREBASE_DATABASE.child("unowned_businesses/\(businessID)/members").queryOrderedByValue().queryEqual(toValue: memberEmail).observe(.childAdded, with: { snapshot in
        
        
            FIREBASE_DATABASE.child("unowned_businesses").child(businessID).child("members").child(snapshot.key).removeValue()
            FIREBASE_DATABASE.child("unowned_businesses/\(businessID)/members").removeAllObservers()
        
        })
        
    }
    
    /// Removes an item from a stores gallery
    func removeGalleryItem(_ object: galleryObject, callback: @escaping (Bool) -> Void){
        
        initPost(requests.deleteImage, params: [
            "business_id": object.businessID as AnyObject,
            "store_id": object.storeID as AnyObject,
            "image_id": object.imageID as AnyObject
            ], callBack: { _ in
            
                callback(true)
                
            }, error: { _ in
        
                callback(false)
                
        })
        
    }
    
    
    /**
     Creates a member for a business
     
     - NOTE: This member will not be a store admin, admin, member or store member. They will only be added to a mailing list which will be deleted when an owner is added to the unowned business.
    */

    func createMember(_ businessID: String, email: String, callback: @escaping (Bool) -> Void){
        
        initPost(requests.createMember, params: [
            "business_id": businessID as AnyObject,
            "email_address": email as AnyObject
            ], callBack: { _ in
                callback(true)
            }, error: { _ in
                callback(false)
        })
        
    }
    
    /**
     Downloads images used in a store gallery
     
     Will only download from where the previous data ends and will only download 10 items at a time. If the number of existing items is not some factor of 10 the function will not download more images.
     
    */
    func downloadGallery(_ object: storeObject, existingData: [galleryObject], callback: @escaping (galleryObject) -> Void){
        
        var startAt: Int?
        
        if !existingData.isEmpty {
            startAt = existingData.last!.created
        }
        
        if existingData.count % 10 == 0 {
            FIREBASE_DATABASE.child("businesses/\(object.businessID)/stores/\(object.id)/images/official").queryOrdered(byChild: "created").queryStarting(atValue: startAt).queryLimited(toFirst: 10).observe(.childAdded, with: { snapshot in
            
                if snapshot.exists(), let data = snapshot.value as? Dictionary<String, AnyObject> {
                    
                    let imageID = snapshot.key
                    var url = "placeholder.jpg"
                    var created = 0
                    
                    if let _val = data["image_url"] as? String {
                        url = _val
                    }
                    
                    if let _val = data["created"] as? Int {
                        created = _val
                    }
                    
                    callback(galleryObject(businessID: object.businessID, storeID: object.id, imageID: imageID, url: url, created: created))
                    
                }
            
            })
        }
        
        
        
    }
    
    /// Updates the location for a store
    func updateLocation(_ object: storeObject, updateCoords: Bool = false, updateLocationName: Bool = false, callback: @escaping (Bool) -> Void){
        
        var params: Dictionary<String, AnyObject> = [
            "business_id": object.businessID as AnyObject,
            "store_id": object.id as AnyObject,
            "lat": object.lat as AnyObject,
            "long": object.long as AnyObject,
            "location_name": object.locationName as AnyObject,
            "update_coords": false as AnyObject,
            "update_location_name": false as AnyObject
        ]
        
        if updateCoords {
            params.updateValue(true as AnyObject, forKey: "update_coords")
        }
        
        if updateLocationName {
            params.updateValue(true as AnyObject, forKey: "update_location_name")
        }
        
        initPost(requests.updateLocation, params: params, callBack: { success in
            
            callback(true)
            
            }, error: { error in
                callback(false)
        })
        
    }
    
    /// Changes the current owner of a business.
    func changeBusinessOwner(_ businessID: String, email: String, callback: @escaping (Bool) -> Void){
        
        initPost(requests.changeBusinessOwner, params: [
                "business_id": businessID as AnyObject,
                "new_owner_email": email as AnyObject
            ], callBack: { response in
                
                callback(true)
                
            }, error: { error in
                
                callback(false)
                
        })
        
    }
    
    /// Sets the owner of an unowned business
    func setUnownedBusinessOwner(_ businessID: String, email: String, callback: @escaping (Bool) -> Void){
        
        initPost(requests.setUnownedBusinessOwner, params: [
                "business_id": businessID as AnyObject,
                "user_email": email as AnyObject
            ], callBack: { response in
            
                callback(true)
                
            }, error: { error in
        
                callback(false)
                
        })
        
    }
    
    /**
     Used to set the logo of a business.
     
     Will automatically generate a unique url and remove the existing logo from storage
    */
    func setLogo(_ businessID: String, logo: UIImage, progress: @escaping (Double) -> Void, success: @escaping (Bool) -> Void){
        
        let logoImageURL = String().createLogoURL(businessID)
        
        FIREBASE_DATABASE.child("businesses/\(businessID)/logo").observeSingleEvent(of: .value, with: { snapshot in
        
            if snapshot.exists(), let snapshot = snapshot.value as? String {
                
                var data = self.compress(logo, withMinSize: 15 * 1000, toResolution: 200, maintainAspectRatio: false)
                
                let logoUploadTask = FIREBASE_STORAGE.child(logoImageURL).put(data, metadata: self.metaData())
                
                logoUploadTask.observe(.progress, handler: { _progress in
                    
                    if let completion = _progress.progress?.fractionCompleted {
                        progress(completion)
                    }
                    
                })
                
                logoUploadTask.observe(.failure, handler: { _ in
                    success(false)
                })
                
                logoUploadTask.observe(.success, handler: { _ in
                    success(true)
                    FIREBASE_STORAGE.child(snapshot).delete(completion: { _ in })
                })
            }
            
        })
        
    }
    
    func updateHomeImage(_ object: storeObject, image: UIImage, progress: @escaping (Double) -> Void, success: @escaping (Bool) -> Void){
        
        _updateHomeImage(object, image: image, progress: { _progress in
            
            progress(_progress)
            
            }, success: { _success in
                
                if _success {
                    FIREBASE_DATABASE.child("businesses/\(object.businessID)/stores/\(object.id)/home_image").setValue(object.url)
                }
                
                success(_success)
                
        
        })
        
    }
    
    /// Updates the home image for a given store, this does not update the associated URL
    fileprivate func _updateHomeImage(_ object: storeObject, image: UIImage, progress: @escaping (Double) -> Void, success: @escaping (Bool) -> Void){
        
        indicateNetworkActivity()
        
        var data = compress(image, withMinSize: 125 * 1000, toResolution: 800, maintainAspectRatio: true)
        
        let task = FIREBASE_STORAGE.child(object.url).put(data, metadata: metaData())
        
        task.observe(.progress, handler: { _progress in
        
            if let __progress = _progress.progress {
                progress(__progress.fractionCompleted)
            }
            
        })
        
        task.observe(.failure, handler: { _ in
        
            stopNetworkActivity()
            FIREBASE_STORAGE.child(object.url).delete(completion: { _ in })
            success(false)
            
        })
        
        task.observe(.success, handler: { _ in
            
            stopNetworkActivity()
            success(true)
            
        })
        
    }
    
    /// creates an unowned store
    func createUnownedStore(_ object: storeObject, currentStoreID: String, homeImageProgress: @escaping (Double) -> Void, success: @escaping (Bool) -> Void){
        
        guard object.homeImage != nil || object.email != nil || object.phone != nil else {
            return
        }
        
        _updateHomeImage(object, image: object.homeImage!, progress: { value in
            
                homeImageProgress(value)
            
            }, success: { complete in
        
                if complete {
                    
                    self.initPost(requests.createStore, params: [
                        "business_id": object.businessID as AnyObject,
                        "current_store_id": currentStoreID as AnyObject,
                        "new_store_id": object.id as AnyObject,
                        "store_phone": object.phone! as AnyObject,
                        "store_email": object.email! as AnyObject,
                        "store_image_url": object.url as AnyObject,
                        "lat": object.lat as AnyObject,
                        "long": object.long,
                        "location_name": object.locationName,
                        "business_name": object.businessName
                        ], callBack: { response in
                        
                            success(true)
                            
                        }, error: { error in
                            
                            success(false)
                            FIREBASE_STORAGE.child(object.url).delete(completion: { _ in })
                    
                    })
                    
                } else {
                    
                    success(false)
                    
                }
                
        })
        
    }
    
    /// Creates a category for a business
    func createCategory(_ businessID: String, title: String, subtitle: String, callback: @escaping (Bool) -> Void){
        
        initPost(requests.createBusinessCategory, params: [
            "business_id": businessID as AnyObject,
            "title": title as AnyObject,
            "subtitle": subtitle as AnyObject
            ], callBack: { _ in callback (true) }, error: { _ in callback(false) })
        
    }
    
    /// Creates an image object for a stores gallery
    func createImage(_ object: storeObject, image: UIImage, callback: @escaping (Bool) -> Void){
        
        if let userID = getUserID() {
            
            let imageID = FIREBASE_DATABASE.childByAutoId().key
            
            let url = String().officialImageURL(object.businessID, storeID: object.id, imageID: imageID)
            
            indicateNetworkActivity()
            
            var data = compress(image, withMinSize: 150 * 1000, toResolution: 800, maintainAspectRatio: true)
            
            let task = FIREBASE_STORAGE.child(url).put(data, metadata: metaData())
            
            task.observe(.success, handler: { _ in
                
                self.initPost(requests.createImage, params: [
                    "business_id": object.businessID,
                    "store_id": object.id,
                    "image_id": imageID,
                    "image_url": url,
                    "user_id": userID
                    ], callBack: { _ in
                    
                        stopNetworkActivity()
                        callback(true)
                        
                    }, error: { _ in
                
                        stopNetworkActivity()
                        callback(false)
                        
                })
                
            })
            
            task.observe(.failure, handler: { _ in
                stopNetworkActivity()
                callback(false)
                
            })
        }
        
    }
    
    /// Creates an unowned business.
    func createdUnownedBusiness(_ object: unownedBusinessObject, homeImageProgress: @escaping (Double) -> Void, logoProgress: @escaping (Double) -> Void, success: @escaping (Bool) -> Void){
     
        let businessID = FIREBASE_DATABASE.childByAutoId().key
        let storeID = FIREBASE_DATABASE.childByAutoId().key
        
        let homeImageURL = String().createHomeImageURL(businessID, storeID: storeID)
        let logoImageURL = String().createLogoURL(businessID)
        
        var homeImageData = compress(object.homeImage!, withMinSize: 150 * 1000, toResolution: 800, maintainAspectRatio: true)
        var logoImageData = compress(object.logo!, withMinSize: 15 * 1000, toResolution: 200, maintainAspectRatio: false)
        
        let homeImageUploadTask = FIREBASE_STORAGE.child(homeImageURL).put(homeImageData, metadata: metaData())
        
        homeImageUploadTask.observe(.progress, handler: { _progress in
            
            if let completion = _progress.progress?.fractionCompleted {
                homeImageProgress(completion)
            }
            
        
        })
        
        homeImageUploadTask.observe(.failure, handler: { _ in
            homeImageProgress(-1)
        })
        
        homeImageUploadTask.observe(.success, handler: { _ in
            let logoUploadTask = FIREBASE_STORAGE.child(logoImageURL).put(logoImageData, metadata: self.metaData())
            
            logoUploadTask.observe(.progress, handler: { _progress in
            
                if let completion = _progress.progress?.fractionCompleted {
                    logoProgress(completion)
                }
                
            })
            
            logoUploadTask.observe(.failure, handler: { _ in
                FIREBASE_STORAGE.child(homeImageURL).delete(completion: {_ in})
                logoProgress(-1)
            })
            
            logoUploadTask.observe(.success, handler: { _ in
                
                let params = [
                    
                    "business_name": object.name!,
                    "logo": logoImageURL,
                    "lat": "\(object.lat!)",
                    "long": "\(object.long!)",
                    "business_id": businessID,
                    "store_id": storeID,
                    "store_image": homeImageURL,
                    "location_name": object.locationName!,
                    
                    "monday_open": object.mondayOpen!,
                    "monday_close": object.mondayClose!,
                    "tuesday_open": object.tuesdayOpen!,
                    "tuesday_close": object.tuesdayClose!,
                    "wednesday_open": object.wednesdayOpen!,
                    "wednesday_close": object.wednesdayClose!,
                    "thursday_open": object.thursdayOpen!,
                    "thursday_close": object.thursdayClose!,
                    "friday_open": object.fridayOpen!,
                    "friday_close": object.fridayClose!,
                    "saturday_open": object.saturdayOpen!,
                    "saturday_close": object.saturdayClose!,
                    "sunday_open": object.sundayOpen!,
                    "sunday_close": object.sundayClose!,
                    
                    "store_email": object.storeEmail!,
                    "store_phone": object.storePhone!
                    
                ]
                
                print(params)
                
                self.initPost(requests.createUnownedBusiness, params: params, callBack: { object in
                    
                        print(object)
                        success(true)
                        
                    }, error: { error in
                        print(error)
                        success(false)
                        FIREBASE_STORAGE.child(logoImageURL).delete(completion: {_ in})
                        FIREBASE_STORAGE.child(homeImageURL).delete(completion: {_ in})
                })
            })
        })
    }
    
    /// Safely downloads all available image reports.
    func downloadImageReports(_ existingData: [ReportsObject]?, callback: @escaping ((ReportsObject)?) -> Void) {
        
        FIREBASE_DATABASE.child("reports/").queryOrdered(byChild: "age").observe(.childAdded, with: { snapshot in
            
            if snapshot.exists(), let _data = snapshot.value as? Dictionary< String, AnyObject> {
                
                var type : reportTypes = .none
                var isOfficial : Bool = false
                var tags : String = ""
                var storeId : String  = ""
                var imageId : String = ""
                var text :  String = ""
                var age : Int  = 0
                var businessId : String  = ""
                var reporterUid : String  = ""
                var reporteeUid : String  = ""
                let reportKey : String = snapshot.key
                                
                if let _type = _data["type"] as? String {
                    switch _type.lowercased() {
                    case reportTypes.business.rawValue:
                        type = .business
                        break
                    case reportTypes.comment.rawValue:
                        type = .comment
                        break
                    case reportTypes.image.rawValue:
                        type = .image
                        break
                    case reportTypes.item.rawValue:
                        type = .item
                        break
                    case reportTypes.reply.rawValue:
                        type = .reply
                        break
                    case reportTypes.review.rawValue:
                        type = .review
                        break
                    default:
                        break
                    }
                }
                if let _isOfficial = _data["isOfficial"] as? Bool {
                    isOfficial = _isOfficial
                }
                if let _tags = _data["tags"] as? String {
                    tags = _tags
                }
                if let _storeId = _data["store_id"] as? String {
                    storeId = _storeId
                }
                if let _imageId = _data["image_id"] as? String {
                    imageId = _imageId
                }
                if let _text = _data["text"] as? String {
                    text = _text
                }
                if let _age = _data["age"] as? Int {
                    age = _age
                }
                if let _businessId = _data["business_id"] as? String {
                    businessId = _businessId
                }
                if let _reporterUid = _data["reporter_uid"] as? String {
                    reporterUid = _reporterUid
                }
                if let _reporteeUid = _data["reportee_uid"] as? String {
                    reporteeUid = _reporteeUid
                }
                
                if let _type = _data["type"] as? String {
                    
                    switch _type.lowercased() {
                    case reportTypes.business.rawValue:
                        callback(BusinessObject(imageId: imageId, age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, url: "", reportKey: reportKey))
                        break
                    case reportTypes.comment.rawValue:
                        callback(CommentObject(reporteeUid: "", reviewId: "", age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, imageId: imageId, url: "", reportKey: reportKey))
                        break
                    case reportTypes.image.rawValue:
                        callback(ImageObject(imageName: "", imageId: imageId, isOfficial: isOfficial, reporteeUid: reporteeUid, age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, url: "", reportKey: reportKey))
                        break
                    case reportTypes.item.rawValue:
                        callback(ItemObject(ItemGroupeId: "", ItemId: "", ItemType: "", age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, imageId: imageId, url: "", reportKey: reportKey))
                        break
                    case reportTypes.reply.rawValue:
                        callback(ReplyObject(reporteeUid: "", commentId: "", reviewId: "", age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, imageId: imageId, url: "", reportKey: reportKey))
                        break
                    case reportTypes.review.rawValue:
                        callback(ReviewObject(reporteeUid: reporteeUid, age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, imageId: imageId, url: "", reportKey: reportKey))
                        break
                    default:
                        break
                    }
                }
            } else {
                callback(nil)
            }
        })
    }
    
    /// Updates a stores contact details
    func updateStoreContactDetails(_ params: Dictionary<String, String>, callback: @escaping (Bool) -> Void){
        initPost(requests.updateStoreContactDetails, params: params as Dictionary<String, AnyObject>, callBack: { _ in
            callback(true)
            }, error: { _ in
            callback(false)
        })
    }
    
    /// Update business contact details
    func updateBusinessContactDetails(_ businessID: String, title: String, data: Dictionary<String, String?>, callback: @escaping (Bool) -> Void){
        
        var phone = ""
        var email = ""
        var website = ""
        var facebook = ""
        var google = ""
        
        if let _value = data["phone"], _value != "" {
            phone = _value!
        }
        if let _value = data["email"], _value != ""{
            email = _value!
        }
        if let _value = data["website"], _value != ""{
            website = _value!
        }
        if let _value = data["facebook"], _value != ""{
            facebook = _value!
        }
        if let _value = data["google"], _value != ""{
            google = _value!
        }
        
        initPost(requests.updateBusinessContactDetails, params: [
            "business_id": businessID as AnyObject,
            "title": title as AnyObject,
            "google": google as AnyObject,
            "facebook": facebook as AnyObject,
            "website": website as AnyObject,
            "phone": phone as AnyObject,
            "email": email as AnyObject
            ], callBack: { object in
            
                print(object)
                callback(true)
                
            }, error: { error in
                
                callback(false)
                
        })
        
    }
    
    /**
     Compresses an image to the minimum specified size. Size should be entered in KiloBytes
     where 1000 Bytes = 1 KB.
     
     - PARAMETER image: the image to be compressed
     
     - PARAMETER withMinSize: compression in KiloBytes. Defaults to 300.
     
     */
    func compress(_ image: UIImage, withMinSize: Int64 = 300 * 1000, toResolution: CGFloat = 400, maintainAspectRatio: Bool = true) -> Data {
        var _image: Data!
        var compression: CGFloat = 1
        var previousCompression: CGFloat = 2
        
        var imageResized = UIImage().reduceResolution(of: image, to: toResolution, maintainAspectRatio: maintainAspectRatio)
        
        func compressor() -> Data {
            previousCompression = compression
            compression *= 0.9
            _image = UIImageJPEGRepresentation(imageResized, compression)
//            print("compressed @")
//            print(compression)
//            print("byte size")
//            print(Int64(_image.count))
            return (Int64(_image.count) < (withMinSize)) || (previousCompression == compression) ? _image! : compressor()
        }
        
        return compressor()
    }
    
    /// Updates an item group
    func updateItemGroup(_ object: itemGroupObject, updateImage: Bool, progress: @escaping (Double) -> Void, callback: @escaping (Bool) -> Void){
        
        func updateData(){
            initPost(requests.updateItemGroup, params: [
                "business_id": object.businessID as AnyObject,
                "group_id": object.id as AnyObject,
                "title": object.name as AnyObject,
                "url": object.url as AnyObject,
                "type": object.type as AnyObject
                ], callBack: { _ in callback (true) }, error: { _ in callback(false) })
        }
        
        // update the image if needed else just update the text
        updateImage ? {
            self.updateImage(object.logo!, url: object.url, type: "logo", progress: { val in progress(val) }, callback: { success in
                
                success ? updateData() : callback(false)
                
            })
            }() : updateData()
        
    }
    
    fileprivate func updateImage(_ image: UIImage, url: String, type: String = "normal", progress: @escaping (Double) -> Void, callback: @escaping (Bool) -> Void){
        
        var data: Data!
        
        if type == "logo" {
            data = compress(image, withMinSize: 15 * 1000, toResolution: 200, maintainAspectRatio: false)
        } else {
            data = compress(image, withMinSize: 150 * 1000, toResolution: 400, maintainAspectRatio: true)
        }
        
        let task = FIREBASE_STORAGE.child(url).put(data, metadata: metaData())
        indicateNetworkActivity()
        task.observe(.progress, handler: { _val in
        
            if let val = _val.progress?.fractionCompleted {
                progress(val)
            }
        
        })
        
        task.observe(.success, handler: { _ in
            callback(true)
            stopNetworkActivity()
        })
        
        task.observe(.failure, handler: { _ in
            callback(false)
            stopNetworkActivity()
        })
    }
    
    /// Downloads all of the item groups for a business
    func downloadItemGroups(_ businessID: String, itemType: String, callback: @escaping (itemGroupObject) -> Void){
        
        FIREBASE_DATABASE.child("businesses/\(businessID)/\(itemType)").observe(.childAdded, with: { snapshot in
        
            if snapshot.exists(), let data = snapshot.value as? Dictionary<String, AnyObject> {
                var title = ""
                var url = ""
                
                
                if let _val = data["image"] as? String {
                    url = _val
                }
                
                if let _val = data["title"] as? String {
                    title = _val
                }
                
                callback(itemGroupObject(itemGroupID: snapshot.key, businessID: businessID, name: title, url: url, type: itemType))
            }
        
        })
        
    }
    
    /** 
     Downloads the "members" of a business
     
     - NOTE: These members arent actual members but just email addresses for the business.
    */
    
    func downloadMembers(_ businessID: String, callback: @escaping ([String]) -> Void){
        
        FIREBASE_DATABASE.child("unowned_businesses/\(businessID)/members/").observeSingleEvent(of: .value, with: { snapshot in
        
            if snapshot.exists(), let data = snapshot.value as? Dictionary <String, String> {
                
                var temp = [String]()
                
                for item in data.values{
                    temp.append(item)
                }
                
                callback(temp)
                
            }
        
        })
        
    }
    
    /// Downloads the contact details for a store
    func downloadStoreContactDetails(_ businessID: String, storeID: String, callback: @escaping (Dictionary<String, String>) -> Void){
        
        FIREBASE_DATABASE.child("businesses/\(businessID)/stores/\(storeID)/contact_details/Store/").observeSingleEvent(of: .value, with: { snapshot in
        
        
            if snapshot.exists(), let response = snapshot.value as? Dictionary<String, String> {
                
                callback(response)
                
            }
        
        })
        
    }
    
    /// Downloads a businesses contact details
    func downloadBusinessContactDetails(_ businessID: String, callback: @escaping (Dictionary <String , String>) -> Void){
        
        FIREBASE_DATABASE.child("businesses/\(businessID)/contact_details/").observeSingleEvent(of: .value, with: { snapshot in
            
            if snapshot.exists(), let data = snapshot.value as? Dictionary <String, AnyObject>, let key = data.keys.first, let _data = data[key] as? Dictionary <String, String> {
                
                var dict = _data
                dict.updateValue(key, forKey: "name")
                callback(dict)
                
            }
            
        })

    }
    
    /// Convenience function. Downloads a logo given a business ID.
    func downloadLogoForBusinessID(_ businessID: String, callback: @escaping (UIImage) -> Void){
        
        String().logoImageURL(businessID, callback: { url in
        
            self.downloadImage(url, callback: { image in
                callback(image)
            })
        
        })
        
    }
    
    func updateDescription(_ businessID: String, description: String, callback: @escaping (Bool) -> Void){
        
        initPost(requests.updateDescription, params: [
                "business_id": businessID as AnyObject,
                "description": description as AnyObject
            ], callBack: { object in
            
                print(object)
                callback(true)
                
            }, error: { error in
                
                print(error)
                callback(false)
                
        })
        
    }
    
    /// Downloads the description for a business
    func downloadDescription(_ businessID: String, callback: @escaping (String) -> Void){
        
        FIREBASE_DATABASE.child("businesses/\(businessID)/description").observeSingleEvent(of: .value, with: { snapshot in
        
            if snapshot.exists(), let _description = snapshot.value as? String {
                callback(_description)
            }
            
        })
        
    }
    
    /// Downloads all the store IDs and location names for a given business.
    func downloadStoreManifest(_ businessID: String, callback: @escaping ([storeParent]) -> Void){
        
        FIREBASE_DATABASE.child("businesses/\(businessID)/store_manifest").observeSingleEvent(of: .value, with: { snapshot in
        
//            print(snapshot.value)
            
            if snapshot.exists(), let snapshot = snapshot.value as? Dictionary<String, String> {
                
                var _stores = [storeParent]()
                
                for key in snapshot.keys {
                    
                    if let location = snapshot[key] {
                        
                        print("printing location")
                        print(location)
                        _stores.append(storeParent(id: key, locationName: location))
                        
                    }
                    
                }
                
//                _stores.sortInPlace({ $0.locationName > $1.locationName })
                print(_stores)
                callback(_stores)
                
            }
            
        })
        
    }
 
    /// Safely downloads an image. Should the image fail or be corrupted on download it will reattempt to download the image once.
    func downloadImage(_ url: String, callback: @escaping (UIImage) -> Void){
        
        indicateNetworkActivity()
        
        FIREBASE_STORAGE.child(url).data(withMaxSize: MAX_DOWNLOAD_SIZE, completion: { (data, error) in
            if error != nil {
                debugPrint(error)
            } else if let data = data, let image = UIImage(data: data) {
                callback(image)
                stopNetworkActivity()
            } else {
                print("image data corrupted on download completion...")
                print("will reattempt download..")
                
                FIREBASE_STORAGE.child(url).data(withMaxSize: MAX_DOWNLOAD_SIZE, completion: { (data, error) in
                    if error != nil {
                        debugPrint(error)
                    } else if let data = data, let image = UIImage(data: data) {
                        callback(image)
                        stopNetworkActivity()
                    }
                })
                
            }
        })
    }
    
    /// Deletes a report with the given reportID
    func removeReport(_ reportKey: String){
        
        let removeReport = FIREBASE_DATABASE.child("reports/\(reportKey)")
        removeReport.removeValue()
    }
    
    func updateReport(_ resolveArray: ReportsObject, newTags: String, newText: String, newImage: UIImage) {
        let key = resolveArray.reportKey
        FIREBASE_DATABASE.child("reports/\(key)/resolved/").setValue(true)
        FIREBASE_DATABASE.child("reports/\(key)/tags/").setValue(newTags)
        FIREBASE_DATABASE.child("reports/\(key)/text/").setValue(newText)
//        FIREBASE_DATABASE.child("reports/\(key)/image_id").setValue(newImage)
 //       FIREBASE_STORAGE.child("businesses/\(key)/stores/\(resolveArray.storeId)/images/").setValue()
    }
    
    func removeImages(_ resolveArray: ReportsObject) {
        let removeImage = FIREBASE_STORAGE.child(resolveArray.url)
        removeImage.delete { (error) -> Void in
            if (error != nil) {
                print("Uh-oh, an error occurred!")
                debugPrint(error)
            } else {
                print("File deleted successfully")
            }
        }
        FIREBASE_DATABASE.child("businesses/\(resolveArray.businessId)/image_id").setValue(resolveArray.imageId)
    }
    
    
    // MARK: Setup Main VC
    
    /// Used to download most of the data relevant to a store object
    func downloadStoreDashboard(_ data: storeObject, callback: @escaping (storeObject) -> Void, failed: @escaping () -> Void){
        
        initPost(requests.storeDashboard, params: [
                "business_id": data.businessID as AnyObject,
                "store_id": data.id as AnyObject
            ], callBack: { response in
            
                if let response = response as? Dictionary<String, AnyObject> {
                    
                    var geoAddress = ""
                    var locationName = ""
                    var url = "placeholder.jpg"
                    var lat = "0"
                    var long = "0"
                    var likes = 0
                    var reviews = 0
                    var rating: Float = 0
                    
                    if let _val = response["geo_address"] as? String {
                        geoAddress = _val
                    }
                    if let _val = response["location_name"] as? String {
                        locationName = _val
                    }
                    if let _val = response["home_image_url"] as? String {
                        url = _val
                    }
                    if let _val = response["lat"] as? String {
                        lat = _val
                    }
                    if let _val = response["long"] as? String {
                        long = _val
                    }
                    if let _val = response["likes"] as? Int {
                        likes = _val
                    }
                    if let _val = response["reviews"] as? Int {
                        reviews = _val
                    }
                    if let _val = response["rating"] as? Float {
                        rating = _val
                    }
                    
                    callback(storeObject(businessID: data.businessID, storeID: data.id, locationName: locationName, lat: Double(lat)!, long: Double(long)!, url: url, geoAddress: geoAddress, likes: likes, reviews: reviews, rating: rating))
                    
                }
            
            }, error: { error in
        
                failed()
                
        })
        
    }
    
    /// Convenience function to setup the main VC; it calls all other necessary functions and passes back the values
    func downloadDashboard(_ existingVerificationRequests: [VerificationObject]?, verificationCallback: @escaping (VerificationObject) -> Void, existingUnownedBusinesses: [unownedBusinessObject]?, businessCallback: @escaping (unownedBusinessObject) -> Void){
        
        downloadVerificationRequests(existingVerificationRequests, callback: { object in verificationCallback(object)})
        downloadUnownedBusinesses(existingUnownedBusinesses, callback: { object in businessCallback(object)})
        removeExpiredImages()
    }
    
    ///Removes images from the business, stores and images trees that the server is unable to delete
    func removeExpiredImages() {
        
        FIREBASE_DATABASE.child("remove_businesses").observe(.childAdded, with: { snapshot in
        
            if snapshot.exists() {
                FIREBASE_STORAGE.child("businesses/\(snapshot.key)").delete(completion: { _ in })
            }
        
        })
        
        FIREBASE_DATABASE.child("remove_images").observe(.childAdded, with: { snapshot in
        
            if snapshot.exists() , let url = snapshot.value as? String{
             
                FIREBASE_STORAGE.child(url).delete(completion: { _ in })
                
            }
        
        })
        
        FIREBASE_DATABASE.child("remove_store").observe(.childAdded, with: { snapshot in
        
            if snapshot.exists() , let jsonData = snapshot.value as? Dictionary<String,Any> {
                
                if let businessID = jsonData["business_id"] as? String , let storeID = jsonData["store_id"] as? String {
                    
                    FIREBASE_STORAGE.child("businesses/\(businessID)/stores/\(storeID)").delete(completion: { _ in })
                    
                }
                
            }
        
        })
        
    }
    
    ///Downloads unowned business objects from firebase
    func downloadUnownedBusinesses(_ existingData: [unownedBusinessObject]?, callback: @escaping (unownedBusinessObject) -> Void){
        
        FIREBASE_DATABASE.child("unowned_businesses/").observe(.childAdded, with: { snapshot in
            
            if snapshot.exists() {
                
                var name: String! {
                    didSet {
                        sendCallback()
                    }
                }
                var logo: UIImage! {
                    didSet {
                        sendCallback()
                    }
                }
                
                func sendCallback(){
                    if name != nil || logo != nil {
                        
                        let obj = unownedBusinessObject()
                        obj.name = name
                        obj.logo = logo
                        obj.businessID = snapshot.key
                        
                        callback(obj)
                    }
                }
                
                FIREBASE_DATABASE.child("businesses/\(snapshot.key)/name").observe(.value, with: { _name in
                    if _name.exists(), let _name = _name.value as? String {
                        name = _name
                    }
                })
                
                FIREBASE_DATABASE.child("businesses/\(snapshot.key)/logo").observe(.value, with: { _url in
                    if _url.exists(), let _url = _url.value as? String {
                        
                        FIREBASE_STORAGE.child(_url).data(withMaxSize: MAX_DOWNLOAD_SIZE, completion: { (data, error) in
                            if error != nil {
                                debugPrint(error)
                            } else if let data = data, let image = UIImage(data: data) {
                                
                                logo = image
                            }
                        })
                    }
                })
            }
        })
    }
    
    
     // MARK: Verification Calls
    ///Downloads verification requests objects from firebase. Will not evaluate approval and reason.
    func downloadVerificationRequests(_ existingData: [VerificationObject]?, callback: @escaping (VerificationObject) -> Void) {
        
        FIREBASE_DATABASE.child("verification_requests/businesses").observe(.childAdded, with: { snapshot in
            if snapshot.exists(), let _data = snapshot.value as? Dictionary< String, AnyObject> {
                
                var businessId : String  = ""
                var registrationId : String  = ""
                var url : String  = ""
                if let _business_id = snapshot.key as? String {
                    businessId = _business_id
                }
                if let _registration_id = _data["registration_id"] as? String {
                    registrationId = _registration_id
                }
                if let _url = _data["url"] as? String {
                    url = _url
                }
                
                callback(VerificationObject(businessID: businessId, registrationID: registrationId, url: url))
                
            }
        })
    }
    
    /**
     Downloads a document given a particluar URL.
     - returns: May return a url as a string in the callback provided the url that is opened cannot be opened by a web browser.
    */
    func downloadDocument(_ url: String, callback: @escaping ((String)?) -> Void) {
        
        FIREBASE_STORAGE.child(url).downloadURL { (URL, error) in
            if (error != nil) {
                // Handle any errors
            } else if let URL = URL {
                
                if UIApplication.shared.canOpenURL(URL) {
                    UIApplication.shared.openURL(URL)
                } else {
                    callback(URL.absoluteString)
                }
            }
        }
    }
   
    /**
     Sends a post request to the engine to set verification for a particular verification request.
     - NOTE: The server will handle removing the verification request.
    */
    func saveVerification(_ object: VerificationObject) {
        
        initPost(requests.setVerification, params: [
            "business_id": object.businessID as AnyObject,
            "name": "John" as AnyObject,
            "surname": "Doe" as AnyObject,
            "approval": object.approval as AnyObject,
            "reason": object.reason as AnyObject
            ])
    }
    
    /// Removes a particular verification request.
    func removeVerifiedFromList(_ object: VerificationObject) {
        
        FIREBASE_DATABASE.child("verification_requests/businesses/\(object.businessID)").removeValue()
    }
}





















