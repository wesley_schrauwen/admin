//
//  extentions.swift
//  StackleApp
//
//  Created by Stackle005 on 9/5/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit
import Firebase

extension UIButton {
    
    /// Downloads an image given a url and then sets the buttons imageview to the image in the "Normal" state
    func setImageForURL(_ url: String, makeCircle: Bool = false){
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator.frame = self.bounds
        
        self.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        NetworkUtilities.sharedFirebaseInstance.downloadImage(url, callback: { _image in
            
            let newImage = _image
            
            if makeCircle {
                newImage.circleMask
            }
            
            self.setImage(newImage, for: UIControlState())
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
            
        })
    }
    
}

extension UIImageView {
    
    func setImageForURL(_ url: String, makeCircle: Bool = false, callback: ((UIImage) -> Void)?) {
        NetworkUtilities.sharedFirebaseInstance.downloadImage(url, callback: { _image in
        
            let newImage = _image
            
            if makeCircle {
                newImage.circleMask
            }
            
            self.image = newImage
            callback?(newImage)
            
        })
    }
    
    
}

extension UIColor {
    
    /// The grey colour used in the reusable header views
    var headerGrey: UIColor {
        return UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
    }
    
    /// The Stackle Teal Color
    var StackleTeal: UIColor {
        return UIColor(red: 0, green: 128/256, blue: 128/256, alpha: 1)
    }
}

extension UIImage {
    
    
    //reduces the resolution of an image to 500 x 500
    func reduceResolution(of image: UIImage, to newResolution: CGFloat = 400, maintainAspectRatio: Bool = true) -> UIImage {
        
        // ensure the largest parameter is used to determine the ratio
        let ratio: CGFloat = newResolution / ((image.size.width >= image.size.height) && maintainAspectRatio ? image.size.width : image.size.height)
        
        // get the new parameters
        let newHeight = ratio * image.size.height
        let newWidth = ratio * image.size.width
        
        
        //draw the image into a temp variable
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
        
    }
    
    var circleMask: UIImage {
        
        let square = CGSize(width: min(100, 100), height: min(100, 100))
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = .scaleToFill
        imageView.image = self
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
}

extension String {
    
    /// Determines if the given string is number safe. Only works on one character at a time.
    var isValidTextFieldNumber: Bool {
        
        let char = self.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        return Int(self) != nil
        
    }
    
    /// Generates a new url for an item manifest image
    func createItemManifestImageURL(_ businessID: String, type: String, groupID: String) -> String {
        return "businesses/\(businessID)/\(type)/\(groupID)/items/\(FIREBASE_DATABASE.childByAutoId().key).jpg"
    }
    
    /// Generates a new url for an item group image
    func createItemGroupImageURL(_ businessID: String, type: String, groupID: String) -> String {
        return "businesses/\(businessID)/\(type)/\(groupID)/\(FIREBASE_DATABASE.childByAutoId().key).jpg"
    }
    
    /// Generates a new home image url for a store
    func createHomeImageURL(_ businessID: String, storeID: String) -> String {
        return "businesses/\(businessID)/stores/\(storeID)/\(FIREBASE_DATABASE.childByAutoId().key).jpg"
    }
    
    /// Generates a unique url for a logo
    func createLogoURL(_ businessID: String) -> String {
        return "businesses/\(businessID)/\(FIREBASE_DATABASE.childByAutoId().key).jpg"
    }
    ///returns the url for an official image
    func officialImageURL(_ businessID: String, storeID: String, imageID: String) -> String {
        print(imageID)
        return "businesses/\(businessID)/stores/\(storeID)/images/\(imageID).jpg"
    }
    ///returns the url for a review image
    func reviewImageURL(_ businessID: String, storeID: String, imageID: String) -> String {
        return "businesses/\(businessID)/stores/\(storeID)/reviews/\(imageID).jpg"
    }
    
    ///returns the url for a businesses logo
    func logoImageURL(_ businessId: String, callback: @escaping (String) -> Void) {
        FIREBASE_DATABASE.child("businesses/\(businessId)/logo").observeSingleEvent(of: .value, with: { snapshot in
        
            if snapshot.exists(), let url = snapshot.value as? String {
                callback(url)
            }
        
        })
    }
    
//    mutating func logoImageURL(businessId: String) {
//        FIREBASE_DATABASE.child("businesses/\(businessId)/logo").observeEventType(.Value, withBlock: { snapshot in
//            if let logoURLString = (snapshot.value) as? String {
//                self = logoURLString
//            }}, withCancelBlock: { error in
//                print(error.description)
//        })
//      //  return ("\(logoURLString)")
//    }
}
