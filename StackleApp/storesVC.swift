//
//  storesVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/02.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class storesVC: storeParentVC, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var stores = [storeParent]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        network.downloadStoreManifest(data.businessID!, callback: { data in
            
            self.stores = data
            self.tableView.reloadData()
            
        })
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "oneLabelTVCell") {
            
            cell.textLabel?.text = stores[indexPath.row].locationName
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.storeData = storeObject(businessID: data.businessID!, storeID: stores[indexPath.row].id, locationName: stores[indexPath.row].locationName, businessName: data.name!)
        performSegue(withIdentifier: "manageStoreSegue", sender: nil)
        
    }

    // MARK: Action
    @IBAction func addStoreButton(_ sender: AnyObject) {
        
        if stores.count != 0 {
            self.storeData = storeObject(businessID: data.businessID!, storeID: stores[0].id, locationName: stores[0].locationName, businessName: data.name!)
            performSegue(withIdentifier: "addStoreSegue", sender: nil)
        }
        
    }
    
}
