//
//  HeaderReusableView.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/28.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class HeaderReusableView: UICollectionReusableView {
        
    @IBOutlet weak var headerLabel: UILabel!
    
    func setupView(_ header: String){
        headerLabel.text = header
        self.backgroundColor = UIColor().headerGrey
    }
    
}
