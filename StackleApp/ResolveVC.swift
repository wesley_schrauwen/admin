//
//  ResolveVC.swift
//  StackleApp
//
//  Created by Stackle005 on 9/26/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit
import Firebase

class ResolveVC: ResolveReportVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var Lbl1: UILabel!
    @IBOutlet weak var Lbl2: UILabel!
    @IBOutlet weak var Lbl3: UILabel!
    @IBOutlet weak var Lbl4: UILabel!
    @IBOutlet weak var businessId: UILabel!
    @IBOutlet weak var storeId: UILabel!
    @IBOutlet weak var tagsText: UITextField!
    @IBOutlet weak var textText: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        navigationItem.titleView = UIImageView(image: UIImage(named: "AppLogo"))
        
        businessId.text = report.businessId
        storeId.text = report.storeId
        tagsText.text = report.tags
        textText.text = report.text
        
    }
    
    @IBAction func imageButtonPressed(_ sender: AnyObject) {
        alerts().resolveAlert(self, reportType: report.type, imagePicker: &imagePicker, callback: {
            // TODO CODE
        })
    }
    
    @IBAction func updateButtonPressed(_ sender: AnyObject) {
        
        guard tagsText.text != nil && textText.text != nil && imageView.image != nil else {
            return
        }
        
        network.updateReport(report, newTags: tagsText.text!, newText: textText.text!, newImage: imageView.image!)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imageView.image = image
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}


















