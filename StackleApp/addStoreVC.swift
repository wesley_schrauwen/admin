//
//  addStoreVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/03.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class addStoreVC: storeParentVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var coordsLabel: UILabel!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var homeImageView: UIButton!
    @IBOutlet weak var locationTextfield: UITextField!
    @IBOutlet weak var phoneNumberTextfield: UITextField!
    
    fileprivate var lat: Double?
    fileprivate var long: Double?
    
    fileprivate var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        emailTextfield.delegate = self
        locationTextfield.delegate = self
        phoneNumberTextfield.delegate = self
        
    }
    
    // MARK: Textfield
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return textField == phoneNumberTextfield ? string.isValidTextFieldNumber : true
        
    }
    
    // MARK: Delegate
    
    func coordsSet(_ lat: Double, long: Double) {
        
        self.lat = lat
        self.long = long
        coordsLabel.text = "\(Int(lat)) / \(Int(long))"
    }
    
    // MARK: Image Picker
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        self.dismiss(animated: true, completion: nil)
        homeImageView.setImage(image, for: UIControlState())
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Action
    @IBAction func setLocationButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "getCoordsSegue", sender: nil)
    }

    @IBAction func setHomeImageButton(_ sender: AnyObject) {
        alerts().imagePickerActionSheet(self, imagePicker: &imagePicker)
    }
    
    @IBAction func saveButton(_ sender: AnyObject) {
        
        guard lat != nil || long != nil || homeImageView.imageView != nil || locationTextfield.text != "" || emailTextfield.text != "" || phoneNumberTextfield.text != "" || locationTextfield.text != nil || emailTextfield.text != nil || phoneNumberTextfield.text != nil else {
            return
        }
        
        let newStoreID = FIREBASE_DATABASE.childByAutoId().key
        
        let obj = storeObject(businessID: data.businessID!, storeID: newStoreID, locationName: locationTextfield.text!, email: emailTextfield.text!, phone: phoneNumberTextfield.text!, lat: lat!, long: long!, url: String().createHomeImageURL(data.businessID!, storeID: newStoreID), businessName: data.name!)
        obj.setHomeImage(homeImageView.imageView!.image!)
        
            network.createUnownedStore(obj, currentStoreID: storeData.id, homeImageProgress: { progress in
                
                self.homeImageView.isUserInteractionEnabled = false
                self.homeImageView.alpha = CGFloat(progress)
                
                }, success: { success in
            
                    toast().success(self, success: success)
                    
            })
    }
    
}
