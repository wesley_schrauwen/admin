//
//  ViewController.swift
//  StackleApp
//
//  Created by Stackle005 on 8/30/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import UIKit
import Firebase

class ReportMainVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableReportsView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
  
    fileprivate var businessReports = [BusinessObject]()
    fileprivate var commentReports = [CommentObject]()
    fileprivate var imageReports = [ImageObject]()
    fileprivate var itemReports = [ItemObject]()
    fileprivate var replyReports = [ReplyObject]()
    fileprivate var reviewReports = [ReviewObject]()
    fileprivate var sectionTitles = [String]()
    
    /// The type of report to show
    var reportToShow: reportTypes = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableReportsView.dataSource = self
        tableReportsView.delegate = self
        
        let logo = UIImage(named: "AppLogo")
        navigationItem.titleView = UIImageView(image: logo)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NetworkUtilities.sharedFirebaseInstance.downloadImageReports(imageReports, callback: { object in
            if let object = object {
                
                if !self.sectionTitles.contains(object.type.rawValue){
                    self.sectionTitles.append(object.type.rawValue)
                }
                switch object.type {
                case .business:
                    self.businessReports.append((object as? BusinessObject)!)
                    break
                case .comment:
                    self.commentReports.append((object as? CommentObject)!)
                    break
                case .image:
                    self.imageReports.append((object as? ImageObject)!)
                    break
                case .item:
                    self.itemReports.append((object as? ItemObject)!)
                    break
                case .reply:
                    self.replyReports.append((object as? ReplyObject)!)
                    break
                case .review:
                    self.reviewReports.append((object as? ReviewObject)!)
                    break
               default:
                    break
                }
                self.tableReportsView.reloadData()
            }
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
            switch self.sectionTitles[section] {
            case reportTypes.business.rawValue:
                return businessReports.count
            case reportTypes.comment.rawValue:
                return commentReports.count
            case reportTypes.image.rawValue:
                return imageReports.count
            case reportTypes.item.rawValue:
                return itemReports.count
            case reportTypes.reply.rawValue:
                return replyReports.count
            case reportTypes.review.rawValue:
                return reviewReports.count
            default:
                return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            if let tabelCell = tableReportsView.dequeueReusableCell(withIdentifier: "OneImageOneLabel") as? OneImageOneLabel {
                                
                switch sectionTitles[indexPath.section]{
                    
                case reportTypes.business.rawValue:
                    tabelCell.setupCell(businessReports[indexPath.row], index: indexPath, callback: { image in
                    })
                    break
                case reportTypes.comment.rawValue:
                    tabelCell.setupCell(commentReports[indexPath.row], index: indexPath, callback: { image in
                        self.commentReports[indexPath.row].setImage(image)
                    })
                    break
                case reportTypes.image.rawValue:
                    tabelCell.setupCell(imageReports[indexPath.row], index: indexPath, callback: { image in
                        self.imageReports[indexPath.row].setImage(image)
                    })
                    break
                case reportTypes.item.rawValue:
                    tabelCell.setupCell(itemReports[indexPath.row], index: indexPath, callback: { image in
                        self.itemReports[indexPath.row].setImage(image)
                    })
                    break
                case reportTypes.reply.rawValue:
                    tabelCell.setupCell(replyReports[indexPath.row], index: indexPath, callback: { image in
                        self.replyReports[indexPath.row].setImage(image)
                    })
                    break
                case reportTypes.review.rawValue:
                    tabelCell.setupCell(reviewReports[indexPath.row], index: indexPath, callback: { image in
                        self.reviewReports[indexPath.row].setImage(image)
                    })
                    break
                default:
                    break
                }
                return tabelCell
            }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            switch sectionTitles[indexPath.section]{
                    
                case reportTypes.business.rawValue:
                    self.performSegue(withIdentifier: "businessReportSegue", sender: businessReports[indexPath.row])
                    break
                case reportTypes.comment.rawValue:
                    self.performSegue(withIdentifier: "commentReportSegue", sender: commentReports[indexPath.row])
                    break
                case reportTypes.image.rawValue:
                    self.performSegue(withIdentifier: "imageReportSegue", sender: imageReports[indexPath.row])
                    break
                case reportTypes.item.rawValue:
                    self.performSegue(withIdentifier: "itemReportSegue", sender: itemReports[indexPath.row])
                    break
                case reportTypes.reply.rawValue:
                    self.performSegue(withIdentifier: "replyReportSegue", sender: replyReports[indexPath.row])
                    break
                case reportTypes.review.rawValue:
                    self.performSegue(withIdentifier: "reviewReportSegue", sender: reviewReports[indexPath.row])
                    break
                default:
                    break
            }
        
    }

    
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? ResolveReportVC {
            vc.report = sender as! ReportsObject
        }
        
    }
}


























