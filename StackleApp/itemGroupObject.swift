//
//  itemGroupObject.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class itemGroupObject {
    
    fileprivate var _id: String!
    fileprivate var _name: String!
    fileprivate var _businessID: String!
    fileprivate var _logo: UIImage?
    fileprivate var _url: String!
    fileprivate var _type: String!
    
    var type: String {
        return _type
    }
    
    var id: String {
        return _id
    }
    
    var name: String {
        return _name
    }
    
    var businessID: String {
        return _businessID
    }
    
    var logo: UIImage? {
        return _logo
    }
    
    var url: String {
        return _url
    }
    
    init(itemGroupID: String, businessID: String, name: String, url: String, type: String){
        _id = itemGroupID
        _type = type
        _name = name
        _businessID = businessID
        _url = url
    }
    
    func setName(_ name: String){
        _name = name
    }
    
    func setImage(_ image: UIImage){
        _logo = image
        _url = String().createItemGroupImageURL(businessID, type: type, groupID: id)
    }
    
    func updateImage(_ image: UIImage){
        _logo = image
        _url = String().createItemGroupImageURL(businessID, type: type, groupID: id)
    }
    
}
