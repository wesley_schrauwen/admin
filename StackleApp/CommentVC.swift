//
//  CommentVC.swift
//  StackleApp
//
//  Created by Stackle005 on 9/8/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class CommentVC: ResolveReportVC {
    
    @IBOutlet weak var businessId: UILabel!
    @IBOutlet weak var reviewId: UILabel!
    @IBOutlet weak var tags: UILabel!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var resolutionBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tags.text = report.tags
        text.text = report.text
        
    }
    
    @IBAction func resolutionBtnClicked(_ sender: AnyObject) {
        alerts().resolutionAlert(self)
    }
}
