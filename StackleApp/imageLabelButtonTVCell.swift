//
//  imageLabelButtonTVCell.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class imageLabelButtonTVCell: UITableViewCell {

    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    weak var delegate: customDelegate?
    fileprivate var indexPath: IndexPath!
    
    func setupCell(_ cellData: itemGroupObject, indexPath: IndexPath, callback: @escaping (UIImage) -> Void){
        self.indexPath = indexPath
        
        cellLabel.text = cellData.name
        
        if let image = cellData.logo {
            cellImageView.image = image
        } else {
            
            print(cellData.url)
            
            NetworkUtilities.sharedFirebaseInstance.downloadImage(cellData.url, callback: { image in
            
                if self.indexPath == indexPath {
                    self.cellImageView.image = image
                }
                
                callback(image)
            
            })
            
        }
    }
    
    @IBAction func cellButton(_ sender: AnyObject) {
        delegate?.buttonPushed!(indexPath)
    }
    
    
}
