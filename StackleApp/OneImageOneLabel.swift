//
//  OneImageOneLabel.swift
//  StackleApp
//
//  Created by Stackle005 on 8/30/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import UIKit

class OneImageOneLabel: UITableViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellTags: UILabel!
    @IBOutlet weak var cellLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    func setupCell(_ text: String, image: UIImage, tags: String){
        cellLabel.text = text
        cellImage.image = image
        cellTags.text = tags
    }

    // Function used by (MainPage)
    func setupCell(_ cellData: ReportsObject, index: IndexPath, callback: @escaping (UIImage) -> Void){
        
        cellLabel.text = cellData.text
        cellTags.text = cellData.tags
        cellImage.tag == index.row
        
        self.activityIndicator.startAnimating()
        
        NetworkUtilities.sharedFirebaseInstance.downloadImage(cellData.url, callback: { image in
            
            self.activityIndicator.stopAnimating()
            
            let _image = image.circleMask
            callback(_image)
            
            if index.row == self.cellImage.tag {
                self.cellImage.image = _image
            }
            
            
        })
    }
}



