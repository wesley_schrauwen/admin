//
//  categoryVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class categoryVC: unownedParentVC, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var categories = [categoryObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        network.downloadCategories(data.businessID!, callback: { object in
        
            if !self.categories.contains(where: { $0.id == object.id }){
                
                self.categories.append(object)
                self.categories.sort(by: { $0.title < $1.title })
                self.tableView.reloadData()
            }
        
        })
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTVCell") {
            
            cell.textLabel?.text = categories[indexPath.row].subtitle
            cell.detailTextLabel?.text = categories[indexPath.row].title
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        alerts().cautionAlert(self, controllerMessage: alerts.alertMessages.remove, actionTitle: alerts.actionTitles.remove, callback: {
        
            self.network.deleteCategory(self.categories[indexPath.row], callback: { success in
                toast().success(self, success: success)
            })
        
        })
    }
    
    // MARK: Delegate
    
    func categorySelected(_ id: String, category: String, subcategory: String) {
        network.createCategory(data.businessID!, title: category, subtitle: subcategory, callback: { success in
            
            toast().success(self, success: success)
        
        })
    }
    
    // MARK: Action

    @IBAction func addButton(_ sender: AnyObject) {
        
        performSegue(withIdentifier: "addCategorySegue", sender: nil)
        
    }
}
