//
//  storeParent.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/02.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class storeParent {
    
    fileprivate var _id: String!
    fileprivate var _locationName: String!
    
    var id: String {
        return _id
    }
    
    var locationName: String {
        return _locationName
    }
    
    init(id: String, locationName: String){
        _id = id
        _locationName = locationName
    }
    
    func updateLocationName(_ name: String){
        _locationName = name
    }
    
}
