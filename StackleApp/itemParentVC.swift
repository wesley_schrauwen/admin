//
//  itemParentVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class itemParentVC: unownedParentVC {

    var itemType: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard itemType != nil else {
            self.navigationController?.popViewController(animated: true)
            return
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let vc = segue.destination as? itemParentVC {
            vc.itemType = itemType
        }
    }

}
