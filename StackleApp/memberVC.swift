//
//  memberVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/03.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class memberVC: unownedParentVC, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var members = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        network.downloadMembers(data.businessID!, callback: { data in
        
            self.members = data
            self.tableView.reloadData()
        
        })
        
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "memberTVCell") {
            
            cell.textLabel?.text = members[indexPath.row]
            
            return cell
            
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        alerts().cautionAlert(self, controllerMessage: alerts.alertMessages.remove, actionTitle: alerts.actionTitles.remove, callback: {
        
            self.network.removeMember(self.data.businessID!, memberEmail: self.members[indexPath.row])
            self.members.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .right)
            
        })
    }
    
    // MARK: Action
    
    @IBAction func addMemberButton(_ sender: AnyObject) {
        alerts().textFieldAlert(self, message: alerts.alertMessages.addMember, callback: { email in
        
            self.network.createMember(self.data.businessID!, email: email, callback: { success in
                toast().success(self, success: success)
                
                if success {
                    self.members.append(email)
                    self.tableView.insertRows(at: [IndexPath(row: self.members.count-1, section: 0)], with: .left)
                }
                
            })
            
        })
        
    }

}
