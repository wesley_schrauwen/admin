//
//  ResolveReportVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/30.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class ResolveReportVC: ParentVC {

    var report: ReportsObject!

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: nil)
        if let vc = segue.destination as? ResolveReportVC {
            vc.report = report
        }
    }
}
