//
//  businessContactDetailsVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/02.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class businessContactDetailsVC: unownedParentVC, UITextFieldDelegate {

    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var facebookTextfield: UITextField!
    @IBOutlet weak var googleTextfield: UITextField!
    @IBOutlet weak var websiteTextfield: UITextField!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        nameTextfield.delegate = self
        phoneTextfield.delegate = self
        emailTextfield.delegate = self
        facebookTextfield.delegate = self
        googleTextfield.delegate = self
        websiteTextfield.delegate = self
        
        network.downloadBusinessContactDetails(data.businessID!, callback: { dict in
            self.nameTextfield.text = dict["name"]
            self.phoneTextfield.text = dict["phone"]
            self.emailTextfield.text = dict["email"]
            self.facebookTextfield.text = dict["facebook"]
            self.googleTextfield.text = dict["google"]
            self.websiteTextfield.text = dict["website"]
        })
    }
    
    // MARK: textfield
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return textField == phoneTextfield ? string.isValidTextFieldNumber : true
        
    }
    
    // MARK: Action
    
    @IBAction func saveButton(_ sender: AnyObject) {
        
        guard nameTextfield.text != nil || nameTextfield.text == "" else {
            return
        }
        
        network.updateBusinessContactDetails(data.businessID!, title: nameTextfield.text!, data: [
            "phone": phoneTextfield.text,
            "website": websiteTextfield.text,
            "facebook": facebookTextfield.text,
            "google": googleTextfield.text,
            "email": emailTextfield.text
            ], callback: { success in
        
                toast().success(self, success: success)
        
        })
        
    }

}
