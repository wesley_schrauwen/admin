//
//  storeObject.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/03.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class storeObject: storeParent {
    
    fileprivate var _homeImage: UIImage?
    fileprivate var _email: String?
    fileprivate var _phone: String?
    fileprivate var _lat: Double!
    fileprivate var _long: Double!
    fileprivate var _businessID: String!
    fileprivate var _url: String!
    fileprivate var _businessName: String!
    fileprivate var _geoAddress: String!
    fileprivate var _likes: Int!
    fileprivate var _reviews: Int!
    fileprivate var _rating: Float!
    
    var likes: Int {
        return _likes
    }
    
    var reviews: Int {
        return _reviews
    }
    
    var rating: Float {
        return _rating
    }
    
    var geoAddress: String {
        return _geoAddress
    }
    
    var businessName: String {
        return _businessName
    }
    
    var url: String {
        
        return _url
        
    }
    
    var homeImage: UIImage? {
        return _homeImage
    }
    
    var email: String? {
        return _email
    }
    
    var phone: String? {
        return _phone
    }
    
    var lat: Double {
        return _lat
    }
    
    var long: Double {
        return _long
    }
    
    var businessID: String {
        return _businessID
    }
    
    /// When initialising the store from memory or when creating a new store
    init(businessID: String, storeID: String, locationName: String, email: String, phone: String, lat: Double, long: Double, url: String, businessName: String) {
        super.init(id: storeID, locationName: locationName)
        
        _url = url
        _businessID = businessID
        _email = email
        _phone = phone
        _long = long
        _lat = lat
        _businessName = businessName
    }
    
    /// When managing a store. Contact Details are provided at a later VC
    init(businessID: String, storeID: String, locationName: String, lat: Double, long: Double, url: String, geoAddress: String, likes: Int, reviews: Int, rating: Float){
        super.init(id: storeID, locationName: locationName)
        _url = url
        _businessID = businessID
        _lat = lat
        _long = long
        _geoAddress = geoAddress
        _likes = likes
        _reviews = reviews
        _rating = rating
    }
    
    
    /// Used when moving to the manageStoreVC. There needs to be a temporary object for the VC to set itself up.
    init(businessID: String, storeID: String, locationName: String, businessName: String){
        super.init(id: storeID, locationName: locationName)
        _businessID = businessID
        _businessName = businessName
    }
    
    func setHomeImage(_ image: UIImage){
        _homeImage = image
    }
    
    func createURL(){
        _url = String().createHomeImageURL(businessID, storeID: id)
    }
    
    func updateCoords(_ lat: Double, long: Double){
        _lat = lat
        _long = long
    }
    
}
