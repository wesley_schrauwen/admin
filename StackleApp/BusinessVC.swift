//
//  BusinessVC.swift
//  StackleApp
//
//  Created by Stackle005 on 9/8/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class BusinessVC: ResolveReportVC {

    @IBOutlet weak var relotutionBtn: UIBarButtonItem!
    @IBOutlet weak var businessId: UILabel!
    @IBOutlet weak var businessLogo: UIImageView!
    @IBOutlet weak var businessTagsLbl: UILabel!
    @IBOutlet weak var businessTextLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        businessTagsLbl.text = report.reportKey
        businessTextLbl.text =  report.text
        businessId.text = report.businessId
    
    }
           
    @IBAction func resolutionBtnClicked(_ sender: AnyObject) {
        alerts().resolutionAlert(self)
    }
}
