//
//  imageCVCell.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/03.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class imageCVCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    fileprivate var cellIndex: IndexPath!
    
    func setupCell(_ cellData: galleryObject, indexPath: IndexPath, callback: @escaping (UIImage) -> Void){
        
        cellIndex = indexPath
        if let image = cellData.image {
            cellImage.image = image
            self.activityIndicator.stopAnimating()
        } else {
            
            activityIndicator.startAnimating()
            NetworkUtilities.sharedFirebaseInstance.downloadImage(cellData.url, callback: { image in
            
                if self.cellIndex == indexPath {
                    self.cellImage.image = image
                    self.activityIndicator.stopAnimating()
                }
                
                callback(image)
            
            })
            
        }
        
    }
    
}
