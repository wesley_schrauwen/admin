//
//  ReplyVC.swift
//  StackleApp
//
//  Created by Stackle005 on 9/8/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class ReplyVC: ResolveReportVC {

    @IBOutlet weak var replyMainImage: UIImageView!
    @IBOutlet weak var replyLbl: UILabel!
    @IBOutlet weak var resolutionBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        replyLbl.text = report.text
        
    }
    
    @IBAction func resolutionBtnClicked(_ sender: AnyObject) {
        alerts().resolutionAlert(self)
    }
}
