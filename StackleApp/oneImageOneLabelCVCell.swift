//
//  oneImageOneLabelCVCell.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/09/28.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class oneImageOneLabelCVCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    /// Used in main VC to determine which report to segue to.
    var cellData: reportTypes!
    
    var cellIndex: IndexPath!
    
    func setupCell(_ cellData: reportTypes){
        
        self.cellData = cellData
        cellLabel.text = cellData.rawValue.capitalized
        cellImageView.image = UIImage(named: "PlaceHolder")
    }
    
    func setupCell(_ cellData: VerificationObject, index: IndexPath, callback: @escaping (UIImage) -> Void){
        
        cellLabel.text = cellData.registrationID.capitalized
        cellIndex = index
        
        if let image = cellData.logo {
            cellImageView.image = image
            self.activityIndicator.stopAnimating()
        } else {
            
            activityIndicator.startAnimating()
            NetworkUtilities.sharedFirebaseInstance.downloadLogoForBusinessID(cellData.businessID, callback: { image in

                if self.cellIndex == index {
                    callback(image)
                    self.cellImageView.image = image
                    self.activityIndicator.stopAnimating()
                }
                
            })
            
        }
        
    }
    
    func setupCell(_ cellData: unownedBusinessObject, index: IndexPath, callback: @escaping (UIImage) -> Void ){
        
        cellLabel.text = cellData.name
        cellIndex = index
        
        if let image = cellData.logo {
            cellImageView.image = image
            self.activityIndicator.stopAnimating()
        } else if let businessID = cellData.businessID {
            
            activityIndicator.startAnimating()
            NetworkUtilities.sharedFirebaseInstance.downloadLogoForBusinessID(businessID, callback: { image in
                
                if self.cellIndex == index {
                    self.cellImageView.image = image
                    callback(image)
                    self.activityIndicator.stopAnimating()
                }
                
            
            })
            
        }
        
    }
    
}
