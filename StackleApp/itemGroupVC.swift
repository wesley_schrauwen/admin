//
//  itemGroupVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class itemGroupVC: itemParentVC, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var items = [itemGroupObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        navigationBar.title = itemType
        
        network.downloadItemGroups(data.businessID!, itemType: itemType, callback: { group in
        
            if !self.items.contains(where: { $0.id == group.id }) {
                self.items.append(group)
                self.tableView.insertRows(at: [IndexPath(row: self.items.count - 1, section: 0)], with: .none)
            }
        
        })
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        FIREBASE_DATABASE.child("businesses/\(data.businessID!)/\(itemType)").removeAllObservers()
        
    }
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let vc = segue.destination as? itemManifestVC, let object = sender as? itemGroupObject {
            vc.group = object
        } else if let vc = segue.destination as? editItemGroupVC, let object = sender as? itemGroupObject {
            
            vc.itemGroup = object
        
        }
        
    }
    
    // MARK: Action
    
    @IBAction func createItemGroupButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "editItemGroupSegue", sender: nil)
    }
    
    // MARK: Delegate
    func buttonPushed(_ indexPath: IndexPath) {
        
        alerts().editItemAlert(self, update: {
                self.performSegue(withIdentifier: "editItemGroupSegue", sender: self.items[indexPath.row])
            }, remove: {
                
                alerts().cautionAlert(self, controllerMessage: alerts.alertMessages.remove, actionTitle: alerts.actionTitles.remove, callback: {
                
                    self.network.removeItemGroup(self.items[indexPath.row], callback: { success in
                        toast().success(self, success: success)
                    })
                    
                })
                
        })
        
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "imageLabelButtonTVCell") as? imageLabelButtonTVCell {
            
            
            cell.delegate = self
            cell.setupCell(items[indexPath.row], indexPath: indexPath, callback: { image in
            
                self.items[indexPath.row].setImage(image)
            
            })
            
            return cell
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "itemManifestSegue", sender: items[indexPath.row])
    }
    

}
