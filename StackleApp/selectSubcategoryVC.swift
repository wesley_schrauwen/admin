//
//  selectSubcategoryVC.swift
//  StackleApp
//
//  Created by Wesley Schrauwen on 2016/10/04.
//  Copyright © 2016 Stackle005. All rights reserved.
//

import UIKit

class selectSubcategoryVC: unownedParentVC, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var category: rawCategoryObject!
    
    @IBOutlet weak var titleBar: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        titleBar.title = category.title
        
    }

    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category.subtitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTVCell") {
                        
            cell.textLabel?.text = category.subtitles[indexPath.row]
            return cell
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        alerts().cautionAlert(self, controllerMessage: alerts.alertMessages.addCategory, actionTitle: alerts.actionTitles.add, callback: {
            
            self.dismiss(animated: false, completion: nil)
            self.delegate?.categorySelected!(self.category.id, category: self.category.title, subcategory: self.category.subtitles[indexPath.row])
            
        
        })
    }
    
    // MARK: Action

    @IBAction func cancelButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}
