//
//  ReviewObject.swift
//  StackleApp
//
//  Created by Stackle005 on 9/13/16.
//  Copyright © 2016 Stackle005. All rights reserved.
//
import UIKit

class ReviewObject: ReportsObject {
    
    fileprivate var _reportee_uid : String!
    fileprivate var _image: UIImage!

    var reporteeUid : String {
        return _reportee_uid
    }
    
    init(reporteeUid: String, age: Int, businessId: String,  reporterUid : String, storeId: String, tags: String, text: String, type: reportTypes, imageId: String, url: String, reportKey: String) {
        super.init(age: age, businessId: businessId, reporterUid: reporterUid, storeId: storeId, tags: tags, text: text, type: type, imageId: imageId, url: url, reportKey: reportKey)
        
        self._reportee_uid = reporteeUid
    }
    func setImage(_ image: UIImage) {
        _image = image
    }
}
